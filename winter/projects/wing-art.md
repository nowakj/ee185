# Project: Wing Art

*Instructor Lead: Charles Gadeken*

**Due: Tuesday, March 8, 2020**

The goals and deliverables for this project are:
  1. Gather and prepare all of the icons and text for Fractal Flyer wings
  1. Use these embellishments to make SVG models for every pair of wings in FLIGHT

## Icons and Text

Each Fractal Flyer has two wings. The right wing (facing inwards, towards the
stairs) has two embellishments on it: a few words of text and an icon.

There will be 76 Fractal Flyers. Work with Steve, Phil, and Mark to gather
76 icons and 76 snippets of text (4 words). The icons should be black and
white, for etching. Put the icons in wings/icons and the text in wings/words.txt.

## Make SVG Models for Wings

The file [wings-template.svg](../wings/wings-template.svg) has a template
for one pair of wings. Pair up icons and text to create 76 pairs of wings.
Create a file for each one, numbered appropriately. Save and commit the
wing designs to the git repository.




