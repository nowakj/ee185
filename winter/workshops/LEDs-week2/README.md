# Workshop: Animating Wings

**Date: Monday, January 13, 2020**

## Goals
In this workshop, we'll 

- program a small embedded computer with Python,
- use a breadboard, 
- program LED strips, and
- come up with some LED patterns for the art piece.

## Basic part: Feather board

As a first step, take board and insert it into your
breadboard.  If you've never used a breadboard before, have no fear:
they are quite simple, and [How to Use a
Breadboard](https://www.losant.com/blog/how-to-use-a-breadboard) has a
great summary. You want to insert it so that each row of pins is on a
different side of the board, like so:

![Feather board in breadboard](feather.jpg)

Sticking it into the breadboard serves two purposes. First,
it will make it easier to attach wires to it, especially when you want
to attach more than one wire to the same pin. Second, it protects the
pins so they won't be easily bent if the board is banged up a bit.

Using your USB micro cable, connect your board to your laptop. The
firmware (low-level code running directly on the hardware) controlling
the USB port tells your computer that the board is two kinds of USB
devices.  First, it's a serial port: this lets you type Python
directly on the board using a standard read-eval-print-loop (REPL). It
also lets you read `print()` statements. Second, it's a storage
device: this lets you copy over Python files that will run
automatically when the Feather boots.

Your board should appear as a CIRCUITPY drive on your computer. [The
CIRCUITPY Drive] explains the files you see there and what they do.
In summary, there's a bunch of library files that allow you to
interact with the board from a simple and easy-to-use scripting
language called Python. For example, from Python you can read sensors,
control LEDs, read and write pins.  The board loads
and runs `main.py`. If you copy a new `main.py` to CIRCUITPY, it'll
run. If you cut power to the board, that `main.py` will remain when
you boot it up again.

First thing, you'll want to copy over a bunch of library files that
give you access to the Feather's features. Download 
[circuitpy-libs.zip](circuitpy-libs.zip), unzip it, and copy the 
`lib` folder to CIRCUITPY. Next, download [feather.py](feather.py).
Take a look at `feather.py`. It looks like this:

```
# general blink routine for neopixel
# should work on all boards with an "NEOPIXEL" pin defined in firmware
#
# M.Holliday

import time
import board, neopixel, digitalio

pixel = neopixel.NeoPixel(board.NEOPIXEL, 1, brightness=0.5, auto_write=False)

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b)


def rainbow_cycle(wait):
    for j in range(255):
        pixel_index = (256 // 1) + j
        pixel[0] = wheel(pixel_index & 255)
        pixel.show()
        time.sleep(wait)

######################### MAIN LOOP ##############################
while True:
     rainbow_cycle(0.01)
```

If you are not familiar with Python, there are lots of online
resources about it: ask the instructors if you need some guidance. 

Copy `feather.py` to CIRCUITPY, renaming it to `main.py`.
You'll see that the LED on the board is cycling through colors. 

Let's walk through what the above code does. The core logic
is at the end, where it says `while True:`. This statement
says "do this forever", because True is always true. 

The `rainbow_cycle` function takes a single parameter, called `wait`.
This tells the function how long to wait after displaying each
color.  In the call to it above, on the last line of code, this value is
0.01: this means the function sets the color of the LEDs, the waits
0.01 seconds, or 10 milliseconds. This is the call  `time.sleep`.

The `rainbow_cycle` function has a loop that executes 255 times.
This loop sets the color of the LED. It sets the color by
turning  a number in the range of 0-254 into a color on the 
color wheel, using the `wheel` function.
The LED on board can display any color. It
does so by having three separate LEDs (red, green, blue, or RGB). By
adjusting the ratio of the brightness of those three, it can display
any color. This is because of [the physiology of the human
eye](https://en.wikipedia.org/wiki/RGB_color_model#Physical_principles_for_the_choice_of_red,_green,_and_blue):
RGB LEDs won't look right to a [mantis peacock
shrimp](https://www.livescience.com/42797-mantis-shrimp-sees-color.html),
for example.
The statement `pixel[0] = ` assigns a future RGB value to the one
pixel, while `pixel.show()` actually programs it. 

Given that each iteration of the loop in `rainbow_cycle` waits for 10ms,
and the loop executes 255 times, we'd expect the LED to cycle through
the whole color wheel every 2.55 seconds. Does this seem about right?

## Output and the Command Line

[Connecting to the Serial
Console](https://learn.adafruit.com/welcome-to-circuitpython/kattni-connecting-to-the-serial-console)
has instructions on how to connect to the Feather's serial
console. There's also [Advanced Serial Console on Mac and
Linux](https://learn.adafruit.com/welcome-to-circuitpython/advanced-serial-console-on-mac-and-linux)
if you have a Mac or Linux. This guide tells you how to figure out the
name to use to refer to the serial port. You want to use the `screen`
program to talk to the board. For example, if your Feather is
`/dev/tty.usbmodem141221`, then you want to type `screen
/dev/tty.usbmodem141221 115200`. The parameter `115200` tells
`screen` how fast the serial port is; USB serial ports are usually
at 115,200 bits per second.
If you are running Windows, refer to [Advanced Serial Console on
Windows](https://learn.adafruit.com/welcome-to-circuitpython/advanced-serial-console-on-windows).

Make a copy of `main.py` on your local drive, and modify it to add a
print statement in the main loop, such as

```
######################### MAIN LOOP ##############################
while True:
     rainbow_cycle(0.01)
     print("Completed cycle.")
```

Copy it over to CIRCUITPY and connect to your boards's
serial console. You should see the output.

Let's manually control the LED. Type control-C to stop the board's
Python code and give you a prompt where you can type Python code.
Cribbing from the start of `main.py`, create a NeoPixel and
program it to be `(0, 10, 10)`, which is a dim cyan color. So, type
code like this:

```
pixel = neopixel.NeoPixel(board.NEOPIXEL, 1, brightness=0.5, auto_write=False)
pixel[0] = (0, 10, 10)
pixel.show()
```

Try setting
the brightness to 255: `(0, 255, 255)`. You can barely look at
it. Finally, hit the reset button on the board to start executing
your `main.py` again.

## LED strip

The board has a single NeoPixel LED on it. We want to program
a whole string of them, attached to a wing. NeoPixel LED 
strips have three wires: power, ground, and data.

You need to give the strip 5V power and ground. Find the pin labeled
GND: if the USB connector is pointing up, it's the fourth one down on
the left (longer) row. Find the pin labeled USB: it's the fourth one
down on the right (shorter) row. USB provides 5V, so we can just tap
into this power. USB limits how much you can draw, so we can't power
hundreds of LEDs this way, but a single strip is no problem.

Now the strip is powered, but it also needs to receive commands from
the board. Connect the signal wire to pin A0 (or another pin you
prefer). Modify your copy of `main.py` to create an array of 60 
NeoPixels on this pin (the first to arguments to the function):

```
stick = neopixel.NeoPixel(board.A0, 60, brightness=0.5, auto_write=False)
```

Then modify `rainbow_cycle` to write to all 60 pixels. You can
do this in a for loop:

```
    for j in range(255):
        pixel_index = (256 // 1) + j
        color = wheel(pixel_index & 255)
        for p in range(60):
            pixel[p] = color
        pixel.show()
        time.sleep(wait)
```

Copy `main.py` onto CIRCUITPY and watch them change colors!

## Wing 

Now that you're lighting up an LED strip, attach it to one of the
precut wings. You can use electrical or other tape. Notice how
the LED lights just pass through the material, emerging on the other
edges as a bright "lightsaber" of light. The one exeception is the 
etching pattern: these rough patches on the surface cause the
light to escape.

Right now, your LED strip is displaying a single color. Try displaying
different colors. You can do this using the `wheel` command. Rather
than setting every pixel to the same color, you can compute several
colors and try them. For example, this will set the first and second
halves of the strip to colors on opposite sides of the wheel (+128):

```
    for j in range(255):
        pixel_index = (256 // 1) + j
        color1 = wheel(pixel_index & 255)
        color2 = wheel((pixel_index + 128) & 255)
        for p in range(0, 30):
            pixel[p] = color1
        for p in range(30, 60):
            pixel[p] = color2
        pixel.show()
        time.sleep(wait)
```

Talk with your neighbors and try different color patterns. You can
change the speed of the animation, make it irregular, or do anything
you'd like. If you're having trouble articulating what you'd like to 
do in Python, feel free to ask Phil or one of your neighbors. 
