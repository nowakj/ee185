# FLIGHT: Packard Interactive Light Sculpture Project

This repository stores all of the files, notes, and technical documents
for FLIGHT, the interactive light sculpture for the Packard building at
Stanford Unviersity.  Stanford students
taking EE185, EE285, and CS241, under guidance from course instructors,
engineer an art piece designed by Charles Gadeken. This engineering involves
designing, building, and testing the mechanical, electrical, and software
components of the pieces.

This git repository depends on a few submodule for supporting software
and tools. After you clone this repository, go to the root of the
repository and type

```
$ git submodule init
$ git submodule update
```

This will clone the submodules for the DigiKey KiCAD library (needed
for the Fractal Flyer printed circuit board) and for Heronart's
LXStudio package (needed for the GUI that controls and animates
the simulation).


The directory structure:
  - autumn: materials from the Autumn offering of EE185.
  - winter: materials from the Winter offering of EE185/EE285/CS241.
  - spring: materials from the Spring offering of EE185/EE285/CS241.
  - FRACTAL\_FLYER: all technical information on Fractal Flyers, the moving, illuminated objects that form FLIGHT. 76 Fractal Flyers are hung in Packard to form the complete FLIGHT installation.
  - software: the software to control and visualize FLIGHT, including firmware, UI, and simulation.
  - packard: architectural drawings and models of Packard, including its stairwell
