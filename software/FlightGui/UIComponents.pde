import java.io.File;
import java.util.List;
import javax.swing.JFileChooser;
import model.*;
import engine.*;

/**
 * The FLIGHT GUI window that lets you move where a Fractal Flyer is
 * physically placed and save the current physical configuration.
 *
 * The configuration is saved to and loaded from a JSON file whose
 * name is defined by Config.FLYER_CONFIG_FILE.  When the file is
 * saved, the previous one is saved as a backup.
 *
 * Physical placement is first determined which "face" the flyer is
 * placed on: C is hanging from the ceiling pipes, F is along the
 * front window, E is the external edge of the central stair, and I is
 * the internal edge of the central stair. Depending on which face the
 * flyer is placed on, different parameters are available for
 * placement. For example, if you are hanging a flyer from the
 * ceiling, you specify which pipe; if you are hanging it off the from
 * window, you specify which vertical bar.
 *
 *
 *  @author Philip Levis <pal@cs.stanford.edu>
 *
 */

class FlyerLayoutControlWindow extends UIWindow {
  final static int WIDTH     = 200;
  final static int HEIGHT    = 420;
  final static int BORDER    =   4;
  final static int ROW_HEIGHT = 28;
  private UIButton enableButton;
  private UIIntegerBox flyerSelect;
  private UISlider  hangDistanceSlider; // All faces
  private UISlider  tiltSlider; // All faces
  private UISlider  directionSlider; // All faces
  private UISlider xSlider; // Front window: x along row
  private UIToggleSet yToggle; // Front window: which row
  private UISlider  positionSlider; // Spirals: point along spiral
  private UIToggleSet  pipeToggle; // Ceiling: which pipe
  private UISlider  mountPointSlider; // Ceiling: were on pipe


  private UIToggleSet faceToggle;
  private UILabel name;
  private UILabel text;

  void updateEditableParameters() {
      try {
          String face = faceToggle.getSelectedOption();
          // Every mounting point has a hang distance
          hangDistanceSlider.setVisible(true);
          tiltSlider.setVisible(true);
          directionSlider.setVisible(true);
          
          // Other measurements are face-dependent; only make relevant
          // ones visible.
          xSlider.setVisible(false);
          yToggle.setVisible(false);
          pipeToggle.setVisible(false);
          positionSlider.setVisible(false);
          mountPointSlider.setVisible(false);
          
          if (face.equals("C")) {
              pipeToggle.setVisible(true);
              mountPointSlider.setVisible(true);
          } else if (face.equals("F")) {
              xSlider.setVisible(true);
              yToggle.setVisible(true);
          } else if (face.equals("I")) {
              positionSlider.setVisible(true);
          } else if (face.equals("E")) {
              positionSlider.setVisible(true);
          } else {
              System.err.println("Unrecognized face: " + face + "; disable all controls.");
          }
      } catch (NullPointerException ex) {
        // There's something wrong with the initialization sequence.
        // Sometimes this is called before all of the different UI
        // elements have been initialized, so a NPI is thrown.
        // Just catch it here - it only happens at startup.
          System.out.println("Caught NPE at startup -- this is normal and recoverable, but would be good to eventually make this bug go away. -pal");
      }
  }


  FlyerLayoutControlWindow(UI ui) {
      super(ui, "LAYOUT TOOL",
            BORDER, FlightGui.this.height - (HEIGHT + BORDER + FlightGui.BOTTOM_ROW_HEIGHT),
            WIDTH, HEIGHT);

    makeFlyerSelect();
    makeEnableButton();
    makeFaceToggle();
    makeTiltSlider();
    makeDirectionSlider();
    makeHangLengthSlider();
    makeXSlider();
    makeYToggle();
    makePositionSlider();
    makePipeToggle();
    makeMountingSlider();

    name = new UILabel(0, 0, 0, 16);
    name.setLabel("?");

    text = new UILabel(0, 0, 0, 16);
    text.setLabel("?");

    float yPos = TITLE_LABEL_HEIGHT;

    yPos += ROW_HEIGHT;

    yPos = labelRow(yPos, "FLYER #", flyerSelect);
    yPos = labelRow(yPos, "NAME", name);
    yPos = labelRow(yPos, "TEXT", text);
    yPos = labelRow(yPos, "FACE", faceToggle);
    yPos = labelRow(yPos, "HANG", hangDistanceSlider);
    yPos = labelRow(yPos, "TILT", tiltSlider);
    yPos = labelRow(yPos, "DIR", directionSlider);
    yPos = labelRow(yPos, "X", xSlider);
    yPos = labelRow(yPos, "Y", yToggle);
    yPos = labelRow(yPos, "POS", positionSlider);
    yPos = labelRow(yPos, "PIPE", pipeToggle);
    yPos = labelRow(yPos, "MOUNT", mountPointSlider);

    makeSaveButton(yPos);

    setFlyer();
    updateEditableParameters();
    setDescription("Tool for physically laying out Fractal Flyers in Packard stairwell");
  }

  private void makeEnableButton() {
    this.enableButton = new UIButton(4, TITLE_LABEL_HEIGHT, width-8, 20) {
      public void onToggle(boolean enabled) {
        if (enabled) {
          flyerSelect.focus(new MouseEvent(this, 0, 0, 0, 0, 0, 0, 0));
          // When you enable, flash the selected flyer;
          // otherwise, it only flashes when the value changes.
          // If it only flashes when the value changes, then the
          // first time you enable, the selected Flyer flashes,
          // but it doesn't on subsequent enables.
          flyerHighlighter.trigger();
          updateEditableParameters(); 
        }
      }
    };
    this.enableButton.setInactiveLabel("Disabled");
    this.enableButton.setActiveLabel("Enabled");
    this.enableButton.setParameter(flyerHighlighter.enabled);
    this.enableButton.addToContainer(this);
    this.enableButton.setDescription("Enable/disable placement tool");
  }

  private void makeFlyerSelect() {
    this.flyerSelect = new UIIntegerBox().setParameter(flyerHighlighter.flyerIndex);

    LXParameterListener selectListener = new LXParameterListener() {
            public void onParameterChanged(LXParameter parameter) {
                setFlyer();
            }
    };
    flyerHighlighter.flyerIndex.addListener(selectListener);
    this.flyerSelect.setDescription("Which flyer is being placed");
  }

  private void makeFaceToggle() {
    this.faceToggle = new UIToggleSet() {
      protected void onToggle(String value) {
        flyerHighlighter.getConfig().getFaceConfig().face = value;
        updateEditableParameters();
        flyerHighlighter.reloadModel();
      }
    };

    // We have to convert to intern() because of a bug in ToggleSet
    // that compares with == rather than .equals(); if you don't
    // use .intern(), then strings don't match. -pal
    int length = Config.FACE_NAMES.length;
    String options[] = new String[length];
    for (int i = 0; i < length; i++) {
        options[i] = Config.FACE_NAMES[i].intern();
    }
    this.faceToggle.setOptions(options);
  }

  private void makeHangLengthSlider() {
    BoundedParameter hang = new BoundedParameter("Hang", 0, 8 * Geometry.FEET);
    this.hangDistanceSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().getFaceConfig().hangDistance = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.hangDistanceSlider.setParameter(hang);
  }

  private void makeTiltSlider() {
    BoundedParameter tilt = new BoundedParameter("Tilt", -10, 10);
    this.tiltSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().tilt = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.tiltSlider.setParameter(tilt);
  }
    
  private void makeDirectionSlider() {
      BoundedParameter direction = new BoundedParameter("Rotation", 0, 360);
    this.directionSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().rotation = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.directionSlider.setParameter(direction);
  }
    
  private void makeXSlider() {
    BoundedParameter x = new BoundedParameter("X", 0, Geometry.FRONT_WINDOW_WIDTH);
    this.xSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().getFaceConfig().x = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.xSlider.setParameter(x);
  }

  private void makeYToggle() {
    this.yToggle = new UIToggleSet() {
            protected void onToggle(String str) {
                flyerHighlighter.getConfig().getFaceConfig().y = Integer.parseInt(str);
                flyerHighlighter.reloadModel();
            }
        };
    String[] yOptions = {"1".intern(), "2".intern(), "3".intern(), "4".intern(), "5".intern()};
    this.yToggle.setOptions(yOptions);
  }

  private void makePositionSlider() {
    BoundedParameter pos = new BoundedParameter("Position", 0, 0, 1);
    this.positionSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().getFaceConfig().position = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.positionSlider.setParameter(pos);
  }


  private void makeMountingSlider() {
    BoundedParameter mount = new BoundedParameter("Mount", 0, 0, 1);
    this.mountPointSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().getFaceConfig().mountPoint = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.mountPointSlider.setParameter(mount);
  }

  private void makePipeToggle() {
    this.pipeToggle = new UIToggleSet() {
            protected void onToggle(String str) {
                flyerHighlighter.getConfig().getFaceConfig().pipe = Integer.parseInt(str);
                flyerHighlighter.reloadModel();
            }
        };
    String[] pipeOptions = {"0".intern(), "1".intern(), "2".intern(), "3".intern(), "4".intern(), "5".intern(), "6".intern(), "7".intern()};
    pipeToggle.setOptions(pipeOptions);
  }

  // Makes a row in the window, consisting of a label and an assocated
  // UI element.
  private float labelRow(float yPos, String label, UI2dComponent obj) {
    UILabel lobj = new UILabel(4, yPos, 50, 20)
        .setLabel(label);
    lobj.addToContainer(this);
    obj
    .setPosition(58, yPos)
    .setSize(width-62, 20)
    .addToContainer(this);
    yPos += ROW_HEIGHT;
    return yPos;
  }

  private void makeSaveButton(float yPos) {
    UIButton saveButton = new UIButton(4, yPos, this.width-8, 20) {
      public void onToggle(boolean active) {
        if (active) {
          String backupFileName = Config.FLYER_CONFIG_FILE + ".backup." + month() + "." + day() + "." + hour() + "." + minute() + "." + second();
          saveStream(backupFileName, Config.FLYER_CONFIG_FILE);
          FlyerConfig[] configs = model.getFlyerConfigs();
          pengine.getIO().saveConfigFile(flyerConfigurations, Config.FLYER_CONFIG_FILE);
          setLabel("Saved. Restart needed.");
        }
      }
    };
    saveButton.setMomentary(true).setLabel("Save Changes").addToContainer(this);
    saveButton.setDescription("Save current physical layout to " + Config.FLYER_CONFIG_FILE + " and make a backup of the previous layout");
  }
    
  private void setFlyer() {
    FlyerConfig config = flyerConfigurations[flyerHighlighter.flyerIndex.getValuei()];
    hangDistanceSlider.getParameter().setValue(config.getFaceConfig().hangDistance);
    tiltSlider.getParameter().setValue(config.tilt);
    directionSlider.getParameter().setValue(config.rotation);
    xSlider.getParameter().setValue(config.getFaceConfig().x);
    String yVal = Integer.toString(config.getFaceConfig().y).intern();
    yToggle.setSelectedOption(yVal);
    faceToggle.setSelectedOption(config.getFaceConfig().face.intern());
    positionSlider.getParameter().setValue(config.getFaceConfig().position);
    pipeToggle.setSelectedOption(Integer.toString((int)config.getFaceConfig().pipe));
    mountPointSlider.getParameter().setValue(config.getFaceConfig().mountPoint);
    name.setLabel(config.getMetadata().name);
    text.setLabel(config.getMetadata().text);
    updateEditableParameters();
  }
}

/** 
 * A simple wrapper around a UIMixer, sizing it to the right size.
 * This provides the sliders that let you control the "volume"
 * of the different patterns loaded in the channels.
 *
 * @author: Philip Levis <pal@cs.stanford.edu>
 */
class ChannelMixerWindow extends UIWindow {
  final static int WIDTH     = 708;
  final static int HEIGHT    = 180;
  final static int BORDER    =   4;
  private final UIMixer mixer;

    ChannelMixerWindow(LX thislx, UI ui) {
      super(ui, "MIXER",
            300, FlightGui.this.height - (HEIGHT + BORDER + FlightGui.BOTTOM_ROW_HEIGHT),
            WIDTH, HEIGHT);
      this.mixer = new UIMixer(ui, thislx, 0, 0, HEIGHT);
      this.mixer.addToContainer(this);
      this.setDescription("Mixer for controlling which patterns are active, how they are mixed, and their relative strengths. Channel 0 controls wing position.");
    }

    public UIMixer getMixer() {
      return this.mixer;
    }
}

class ClipPlayingWindow extends UIWindow {
  private final String FILENAME = "flight-set.json";
  private final String[] labels = new String[] { "-", "-", "-", "-" };
  private final FlightEngine flightEngine;
  private final UIFileNameBox filenameBox;
  private static final int WIDTH = 150;
  private static final int HEIGHT = 122;
  private static final int BORDER = 4;
    
    ClipPlayingWindow(LX lx, UI ui, FlightEngine engine) {
    super(ui, "PLAYLISTS", FlightGui.this.width-144, FlightGui.this.height - (HEIGHT + BORDER + FlightGui.BOTTOM_ROW_HEIGHT), 140, 122);

    this.flightEngine = engine;
    
    float yPos = TITLE_LABEL_HEIGHT;
    
    final UIButton playButton = new UIButton(6, yPos, 40, 20) {
            protected void onToggle(boolean active) {
                if (active) {
                    flightEngine.playbackPlaylist();
                }
            }
        };
    playButton
    .setLabel("PLAY")
    .addToContainer(this);
      
    final UIButton stopButton = new UIButton(6 + (this.width-8)/3, yPos, 40, 20) {
            protected void onToggle(boolean active) {
                if (active) {
                    flightEngine.stopPlaylist();
                }
            }
        };
    stopButton
    .setMomentary(true)
    .setLabel("STOP")
    .addToContainer(this);
      
    final UIButton recButton = new UIButton(6 + 2*(this.width-8)/3, yPos, 40, 20) {
            protected void onToggle(boolean active) {
                if (active) {
                    flightEngine.startRecording();
                } else {
                    flightEngine.stopRecording();
                }
            }
        };
    recButton
    .setLabel("REC")
    .setActiveColor(0xcc3333)
    .addToContainer(this);
    
    yPos += 24;
    final UIButton loopButton = new UIButton(4, yPos, this.width-8, 20) {
            protected void onToggle(boolean active) {
                flightEngine.setPlaylistLooping(active);
            }
        };
    loopButton
    .setInactiveLabel("One-shot")
    .setActiveLabel("Looping")
    .addToContainer(this);
    
    yPos += 24;
    filenameBox = new UIFileNameBox(4, yPos, this.width-8, 20);
    filenameBox
    .setValue(FILENAME)
    .setEditable(true)
    .setTextAlignment(CENTER, CENTER)
    .setBackgroundColor(#333333)
    .setBorderColor(#666666)
    .addToContainer(this); 
    
   yPos += 24;
    new UIButton(4, yPos, (this.width-12)/2, 20) {
      protected void onToggle(boolean active) {
        if (active) {
            String filename = filenameBox.getValue();
//          String fileName = labels[automationSlot.getValuei()].equals("-") ? "set.json" : labels[automationSlot.getValuei()];
            flightEngine.savePlaylist(filename);
//          selectOutput("Save Set",  "saveSet", new File(dataPath(fileName)), UILoopRecorder.this);
        }
      }
    }
    .setMomentary(true)
    .setLabel("Save")
    .addToContainer(this);
    
    new UIButton(this.width - (this.width-12)/2 - 4, yPos, (this.width-12)/2, 20) {
      protected void onToggle(boolean active) {
        if (active) {
            String filename = filenameBox.getValue();
            flightEngine.loadPlaylist(filename);
        }
      }
    }
    .setMomentary(true)
    .setLabel("Load")
    .addToContainer(this);
    
    final LXParameterListener listener;
   // automationSlot.addListener(listener = new LXParameterListener() {
  //    public void onParameterChanged(LXParameter parameter) {
  //      LXAutomationRecorder auto = automation[automationSlot.getValuei()];
  //      stopButton.setParameter(automationStop[automationSlot.getValuei()]);
  //      playButton.setParameter(auto.isRunning);
  //      armButton.setParameter(auto.armRecord);
  //      loopButton.setParameter(auto.looping);
  //      slotLabel.setLabel(labels[automationSlot.getValuei()]);
  //    }
  //  });
  //  listener.onParameterChanged(null);

//    slotLabel.setLabel(labels[automationSlot.getValuei()] = "Burning Man Playlist.json");
  }

  public void saveSet(File file) {
    if (file != null) {
 //     saveBytes(file.getPath(), automation[automationSlot.getValuei()].toJson().toString().getBytes());
 //     slotLabel.setLabel(labels[automationSlot.getValuei()] = file.getName());
    }
  }
  
  public void loadSet(File file) {
    if (file != null) {
//      String jsonStr = new String(loadBytes(file.getPath()));
//      JsonArray jsonArr = new Gson().fromJson(jsonStr, JsonArray.class);
//      automation[automationSlot.getValuei()].loadJson(jsonArr);
//      slotLabel.setLabel(labels[automationSlot.getValuei()] = file.getName());
    }
  }
}

/**
 * Window that lets you choose the pattern for the currently
 * selected channel in the bottom mixer as well as set the parameters
 * of that Pattern. It queries the FlightEngine for the set of available
 * patterns and provides those as options. A pattern's parameters are
 * displayed as sliders. Heavily copied from SQUARED's UIMultiDeck.
 *
 * A significant portion of the logic in this class is to properly
 * handle automation (playing a playlist). For example, if a playlist
 * changes the pattern on a channel, we want to reflect that in the GUI.
 * Put another way, these GUI elements can be modified both by user
 * interaction as well as scripts, so we need to observe when things change
 * using listeners.
 *
 * @author Philip Levis <pal@cs.stanford.edu>
 */
class PatternControlWindow extends UIWindow {
  // A bunch of configuration constants.
  final static int WIDTH     = 150;
  final static int HEIGHT    = 500;
  final static int BORDER    = 4;
  final static int SLIDERS = 4;
  final static int SLIDER_HEIGHT = 24;
  final static int SLIDER_SPACING = 40;
  final static int NUM_PATTERNS_VISIBLE = 6;
  final static int PATTERN_ROW_HEIGHT = 40;
  final static int PATTERN_LIST_HEIGHT = NUM_PATTERNS_VISIBLE * PATTERN_ROW_HEIGHT;
  final static int TITLE_LABEL_HEIGHT = 25;

  // Store a list of patterns and a listener for each channel.
  final UIItemList.ScrollList[] patternLists;
  final LXChannel.Listener[] channelListeners;

  // Sliders below the pattern list, for modifying the parameters of
  // the selected pattern.
  final UISlider[] sliders;
  final LX thislx;
    
  PatternControlWindow(LX mylx, UI ui) {
      super(ui, "Patterns",
            FlightGui.this.width - (WIDTH + BORDER), 30,
            WIDTH, HEIGHT);
      int yp = TITLE_LABEL_HEIGHT;
      this.thislx = mylx;
      
      List<LXAbstractChannel> channels = thislx.engine.mixer.getChannels();
      int numChannels = channels.size();
      patternLists = new UIItemList.ScrollList[numChannels];
      channelListeners = new LXChannel.Listener[numChannels];
      // Set up the pattern selection list
      for (int i = 0; i < numChannels; i++) {
          LXChannel channel = (LXChannel)channels.get(i);
          int index = channel.getIndex();
          List<UIItemList.Item> items = new ArrayList<UIItemList.Item>();
          for (LXPattern p: channel.getPatterns()) {
              items.add(new PatternScrollItem(channel, p));
          }
          UIItemList.ScrollList list = new UIItemList.ScrollList(ui, 1, yp, this.width - 2, PATTERN_LIST_HEIGHT);
          list.setItems(items);
          list.setVisible(channel.getIndex() == thislx.engine.mixer.focusedChannel.getValuei());
          list.addToContainer(this);
          patternLists[index] = list;
      }

      // Create the sliders for pattern parameters
      yp += patternLists[0].getHeight();
      sliders = new UISlider[SLIDERS];
      for (int si = 0; si < sliders.length; ++si) {
          int x = BORDER;
          int y = yp + (si * SLIDER_SPACING);
          UISlider slider = new UISlider(x, y, WIDTH - 2 * BORDER, SLIDER_HEIGHT);
          slider.setEditable(true);
          slider.addToContainer(this);
          sliders[si] = slider;
      }

      // Set up the pattern selection. We need to listen to channels in case automation
      // (e.g., a playlist) changes the pattern on a channel.
      for (int i = 0; i < numChannels; i++) {
          LXChannel channel = (LXChannel)thislx.engine.mixer.getChannel(i); 
          channelListeners[channel.getIndex()] = new LXChannel.AbstractListener() {
              @Override
              public void patternWillChange(LXChannel channel,
                                            LXPattern pattern,
                                            LXPattern nextPattern) {
                  patternLists[channel.getIndex()].redraw();
              }
                  
              @Override
              /* When the pattern for a channel changes (e.g. due a playlist), then
               *  show this change in the UI. Change the channel name to
               *  the pattern and make that pattern focused in the list. */
              public void patternDidChange(LXChannel channel, LXPattern pattern) {
                  channel.goPattern(pattern);
                  channel.label.setValue(pattern.getClass().getSimpleName());
                  List<LXPattern> patterns = channel.getPatterns();
                  for (int i = 0; i < patterns.size(); ++i) {
                      if (patterns.get(i) == pattern) {
                          patternLists[channel.getIndex()].setFocusIndex(i);
                          break;
                      }
                  }  
                  patternLists[channel.getIndex()].redraw();

                  /* Change the LXParameters associated with the sliders to the parameters of
                   * the newly selected channel. */
                  if (channel.getIndex() == thislx.engine.mixer.focusedChannel.getValuei()) {
                      int pi = 0;
                      for (LXParameter parameter : pattern.getParameters()) {
                          if (pi >= sliders.length) {
                              break;
                          }
                          if (parameter instanceof LXListenableNormalizedParameter) {
                              sliders[pi].setParameter((LXListenableNormalizedParameter)parameter);
                              sliders[pi].setEditable(true);
                              pi++;
                          }
                      }
                      while (pi < sliders.length) {
                          sliders[pi++].setParameter(null);
                      }
                  }
              }
          };
          channel.addListener(channelListeners[channel.getIndex()]);
          channelListeners[channel.getIndex()].patternDidChange(channel, channel.getActivePattern());
      }

      // Registers when the focused channel changes and updates the displayed pattern list to be
      // of that channel.
      thislx.engine.mixer.focusedChannel.addListener(new LXParameterListener() {
              public void onParameterChanged(LXParameter parameter) {
                  try {
                    LXChannel channel = (LXChannel)thislx.engine.mixer.getFocusedChannel();
                    redraw();
                  
                    channelListeners[channel.getIndex()].patternDidChange(channel, channel.getActivePattern());
                  
                    int pi = 0;
                    for (UIItemList.ScrollList patternList : patternLists) {
                        patternList.setVisible(pi == thislx.engine.mixer.focusedChannel.getValuei());
                        pi++;
                    }
                  } catch (ClassCastException ex) {
                    // Some channels in the mixer panel are special ones,
                    // like the master volume; ignore them.
                  }
              }
          });
  
      this.setDescription("Select and configure patterns for the currently selected channel (" + thislx.engine.mixer.getFocusedChannel().getIndex() + ")");
  }

    /**
     * The elements in the list of patterns. This subclass
     * is mostly about giving them the right label based
     * on the underlying class (LXPattern) they wrap around.
     * Heavily copied from SQUARED.
     *
     * @author Philip Levis <pal@cs.stanford.edu>
     */
  private class PatternScrollItem extends UIItemList.Item {

    private final LXChannel channel;
    private final LXPattern pattern;

    private final String label;

    PatternScrollItem(LXChannel channel, LXPattern pattern) {
      this.channel = channel;
      this.pattern = pattern;
      this.label = pattern.getClass().getSimpleName();
    }

    public String getLabel() {
      return this.label;
    }

    public boolean isSelected() {
      return this.channel.getActivePattern() == this.pattern;
    }

    public boolean isPending() {
      return this.channel.getNextPattern() == this.pattern;
    }

    public void onActivate() {
      this.channel.goPattern(this.pattern);
    }

    public void onFocus() {
      this.channel.goPattern(this.pattern);
    }
  }
}

