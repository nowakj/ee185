## Networking ##
Within networking is contained a server.c which contains the code for the database side of the simulation where all 76 flyers communicate with the singular server using a socket to identify how each fractal flyer is changing.  These commands are all processed by the server.c which creates a data structure which stores the state of the fliers.

The server starts by accepting the number of connections equal to the number of fractal fliers and reading a message from each that identifies its fractal flyer num.  They are then placed in an array along with the accept fd.  This array is then polled over to identify any new connections.  If the connections include commands they are parsed and then the changes are saved to the datastructure.  If the connection is an accept, the process identifies which Fractal flyer closed its connection an creates a new connection to replace it.  The state and environment is kept the same with the one exception being the new socket fd.  

For now the server prints its state after every change.  The state will later be connected to openGL

The server is created by dataprocess.cc and closed by it using a SIGKILL sent by dataprocess.cc

The commands are indicated by the following numbers:<br/>
#define BODYLED 1<br/>
#define WINGLED 2<br/>
#define WINGANGLE 3<br/>
These definitions also exist inside simlib.c

Left and Right wings are defined as follows:<br/>
#define LEFT 1<br/>
#define RIGHT 0<br/>
These definitions also exist inside dataprocess.cc

The port used for communication is as follows:<br/>
#define PORT 8080<br/>
This port is also defined inside simlib.c

The number of flyers read in is as follows:<br/>
#define NUM_FLYERS 2<br/>
The same number is defined inside dataprocess.cc
