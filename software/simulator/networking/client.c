// Client side C/C++ program from geeksforgeeks.org
#include <stdio.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <unistd.h> 
#include <string.h> 
#include <errno.h>
#define PORT 0xf100 
   
int main(int argc, char const *argv[]) 
{ 
    int sock = 0, valread; 
    struct sockaddr_in serv_addr; 

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
    { 
        printf("\n Socket creation error \n"); 
        return -1; 
    } 

    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(PORT); 
       
    // Convert IPv4 and IPv6 addresses from text to binary form 
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)  
    { 
        printf("\nInvalid address/ Address not supported \n"); 
        return -1; 
    } 
   
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) 
    { 
        printf("%d",errno);
        printf("\nConnection Failed \n"); 
        return -1; 
    } 

    char *message_send = "Hello from client!!!"; 
    char message_recieved[1024] = {0}; 

    send(sock , message_send , strlen(message_send) , 0 ); 
    printf("Sent message Len: %lu\n",strlen(message_send)); 
    printf("Message sent: %s\n",message_send); 
    valread = read( sock , message_recieved, 1024); 
    printf("Recieved message Len: %d\n",valread);
    printf("Message Recieved: %s\n",message_recieved); 
    return 0; 
} 
