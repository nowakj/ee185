# FLIGHT Simulator

The FLIGHT simulator is a Linux program that appears to be the FLIGHT
installation and responds to scripting commands. It is structured as
follows:

  - [fsimulator](fsimulator): the main executable for running the simulator.
  - [graphics](graphics): the graphics library for displaying the simulated
  state of FLIGHT.
  - [client](client): a simple test client for interacting with the simulator.
  
The intended use of the simulator is to test and visualize what the
[FlightGui](../FlightGui) UI is doing. It allows you to test its
output without requiring a full FLIGHT installation. This lets you
test whether the UI is producing correct scripts and debug more easily.

## Operation

Normally, each Fractal Flyer in FLIGHT is plugged into a computer with
USB. Each Flyer appears to the computer in two ways, as a serial port
presenting a Python read-eval-print-loop and as a removeable USB drive
that you can store Python code on. 

The FLIGHT simulator recreates this behavior. When it boots, it
creates 76 entries in the local file system (named FIFOs) that act
like serial ports. These simulated serial ports are connected to
Python interpreters that provide the same API as a Fractal Flyer (see
below). The simulator also creates 76 directories that you can copy
Python code into, code which is accessible to the Python
interpreters. Just like the Fractal Flyers, if you copy new code to
the directory, the Python interpreter reboots: the simulator registers
for changes to the directory with `inotify(7)` and restarts the
corresponding interpreter process when it receives a notification.

These 76 interpreter processes send commands to a server process over
a network socket, which simulates the lights and motion of the Fractal
Flyers and displays them graphically.

![The high level flow of the simulator is summarized in this diagram.](SimulatorFlow.png)

Currently, the simulation component of the simulator is complete but
the graphics are unfinished.

## Python API

Fractal Flyers provide 3 Python functions to control their lights and wings:

``` Python
wing_leds(LEFT|RIGHT, (r, g, b), (r, g, b,), (r, g, b,))
body_leds((r, g, b)
wing_angle(LEFT|RIGHT, angle)

```

Where r g and b are 0-255 and angle is -90.0 - 90.0.

This API assumes that the LED changes are near-instantaneous. The wing
angle change, however, may take time. Each Fractal Flyer has a local
control loop that moves the wings to the last specified position in a 
stable way.

For example, suppose the left wing is at 0 and a script calls
`wing_angle(LEFT, -20)`. The left wing will start to lower to
-20. Suppose, when it is at -10, the script calls `wing_angle(LEFT,
-30)`.  The wing will keep on lowering until it reaches -30. If, when
it is at -25, the script calls `wing_angle(LEFT, 10)`, it will slow
and stop, then start moving to 10.  The firmware takes care of all of
this logic.

