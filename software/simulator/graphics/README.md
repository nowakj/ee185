This directory contains the visual output of the simulation sytem, rendered 
high-quality graphics in OpenGL. 
The graphics library eventually will continuously reads commands through a socket connected 
to the main simulator data_processor, which itself reads python through 76 
named pipes, written to by the engine's python generator. The graphics library 
translates these commands (specifying wing position, wing led color, and 
body led color) to change the state of the rendered fractal flyer, running 
as a simulation in an OpenGL framework. 


Files: 
**stairwell.blend --> stairwell.obj/stairwell.mtl:** 
These are the files specifying the modeled version of the Packard stairwell. 
The .blend file can be opened in Blender 2.8, and the design/coloring of 
the object can be changed directly. The stairwell is currently rendered with 
basic coloring for most objects, and custom glass, light-reflecting shaders for 
the windows. The object is exported as an .obj file, with a corresponding 
.mtl texture map, specifying the material properites for each object contained
in the stairwell object. 
(https://code.stanford.edu/plevis/ee185/-/blob/master/software/simulator/graphics/Screen_Shot_2020-05-26_at_4.13.08_PM.png)

GraphicsLib/main.cpp: 

This is the main rendering loop for the graphical simulation. It sets up an 
OpenGL instance, specifies a basic flier object (two simple triangles), loads
shaders in for the object, and loads a basic camera and color-changing pattern
for the rendered flier objects. It should be expanded to read in the positions
of fliers from the passed-in flier configuration, and update the graphical
state of the fliers based on commands passed from the main simulator process. 

GraphicsLib/shader.vert,GraphicsLib/shader.frag, GraphicsLib/shaderClass.h: 

These files specify two basic material shaders for rendering the flier objects 
and a class defining a shader object, which appropriately re-directs the output 
of the vertex shader as input of the fragment shader. 

GraphicsLib/cameraClass.h: 

Specifies a camera that can move through a rendered scene and respond to user input 
(mouse scrolling and w-a-s-d keyboard input, corresponding to up-left-down-right 
directions, respectively.)

GraphicsLib/modelClass.h and GraphicsLib/meshClass.h: 

These two classes, taken together, can load in .obj models, apply appropriate 
texturing to the models, and render them as 3D graphics within a scene. The 
model class specifically must be further expanded to render material-based 
objects, in addition to textured objects, in order to render an accurate stairwell
model in this environment. 

GraphicsLib/stb_image.h/stb_image.cpp/glad.c: 

Third-party packages; these libraries are "header only", so no additional 
compilation or linking is required. 

This library is taken almost entirely from the opensource OpenGL tutorial library 
https://learnopengl.com/Introduction, which provides the basic implementation for 
each of these classes. 

To configure this library to work and render graphics, on MacOS (Windows specific
instructions, using VSCode as an IDE can be found at https://learnopengl.com/Getting-started/Creating-a-window) : 

1) Download XCode and the CMake GUI 
2) Open GraphicsLib.xcodeproj in XCode; this will open an XCode IDE that will 
allow you to develop, compile, and run the graphical interface. 
3) Download the following packages: 
    GLFW: 
    https://riptutorial.com/opengl/example/21105/setup-modern-opengl-4-1-on-macos--xcode--glfw-and-glew-#:~:text=Install%20GLFW&text=Download%20and%20install%20Xcode%20from%20Mac%20App%20Store.&text=Open%20CMake%2C%20click%20on%20Browse,Build%20folder%20in%20previous%20step.
    Ensure that the .dylib (libglfw.<VERSION#>.dylib) is linked to GraphicsLib.xcodeproj 

    GLAD: 
    Go to https://glad.dav1d.de/ and generate a C++ OpenGL API with a specified version 
    number (this project was initially configured with Version 4.1). Copy the two 
    "include" folders, glad and KHR, into your OS's include directory (likely 
    /usr/local/include), and add the generated glad.c to the XCode project. 
   
    **Ensure that you have included the appropriate libraries in your XCode environment; 
    if you place headers for all downloaded packages in /usr/local/include, you can simply 
    include the singular path in the project's build configurations, and all libraries 
    will be linked appropriately. ** 
    
    GLM: 
    Download the GLM package from: https://glm.g-truc.net/0.9.8/index.html and copy
    directory into your OS includes folder. 
    
    Assimp: 
    Download from: http://assimp.org/index.php/downloads (this project was configured
    with Version 3.1.1). 
    Open the downloaded binary, and compile the binary using CMake. 
    Next, find libassimp.<VERSIONNUMBER>.dylib in code/Debug or code/Release, and link 
    the binary into your XCode project. Copy the compiled library's "include" folder 
    to /usr/local/include. 

4) Check path names for textures, models, and shaders, and ensure they accurate
for your own machine. 

5) Should steps 1-4 succeed, simply press the "play" button in XCode, and the 
rendered graphics should pop up in a separate window. 

Be sure to clean the build directory between runs (XCode --> Product --> Clean 
Build Directory.)

Contact durvasulasamsara@gmail.com if any problems arise in configuring 
this library in XCode. 

    
    
    
    
    



