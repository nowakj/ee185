//Fragment shader implementation taken from https://learnopengl.com/Lighting/Materials
//Allows color-setting of the object material; likely should be replaced with a dichroic
//material shader implementation. 

#version 330 core
out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D texture1;
uniform sampler2D texture2;

void main()
{
    FragColor = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), 0.2);
}
