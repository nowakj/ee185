# FLIGHT: Packard Interactive Light Sculpture Project

This directory has Java code for the user interface (UI) that animates a
FLIGHT installation. It contains the software that computes the light and
wing positions of FLIGHT and sends those to Fractal Flyers as Python commands.
The GUI for controlling these animations is a Processing sketch that can
be found the [FlightGui](../FlightGui) directory.
The UI is based on [LX Studio](https://lx.studio/), with 
support for playlists and dynamically controlling the piece.

The directory structure:
  - data: files read and written by the engine, this includes the flyer configurations and playlists 
  - javaFormatting: --
  - lib: external java libraries used by the UI and a symlink to the LX studio java libraries 
  - src: java classes used by the UI

## Compiling code

The code in this directory compiles to `flight.jar`, which is then linked and
used by the GUI. To compile `flight.jar`, run `compile.sh` in this directory.
