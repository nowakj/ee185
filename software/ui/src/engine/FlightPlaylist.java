package engine;

import java.io.File;

/**
 * FlightPlaylist is the interface that defines all the functionality
 * needed by the UI in order to record, play, stop, load and save a
 * FLIGHT playlist
 * @author Moses Swai
 */
public interface FlightPlaylist {

    /** Sets up and starts recording a playlist */
    void startRecording();

    /** Stops recording a playlist. */
    void stopRecording();

    /** Starts playback of a loaded playlist */
    void playbackPlaylist();


    /** Specifies whether a playlist should be played once
        or looped. */
    void setPlaylistLooping(boolean looping);
    
    /** Stops recording or playback of a playlist */
    void stopPlaylist();

    /**
     * Loads a playlist from a file
     * @param filename The relative path of the playlist file to be loaded
     */
    void loadPlaylist(String filename);

    /**
     * Loads a playlist from a file
     * @param file The file object of the playlist file to be loaded
     */
    void loadPlaylist(File file);

    /**
     * Saves a loaded or recorded playlist onto a file
     * @param filename The relative path of the playlist file to be saved
     */
    void savePlaylist(String filename);

    /**
     * Saves a loaded or recorded playlist onto a file
     * @param file The file object of the playlist file to be saved
     */
    void savePlaylist(File file);

}
