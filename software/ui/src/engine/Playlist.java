package engine;

import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import heronarts.lx.LX;
import heronarts.lx.clip.LXClip;
import heronarts.lx.mixer.*;
import heronarts.lx.pattern.LXPattern;

/**
 * Playlist class creates and modifies a playlist JsonArray to make
 * it flexible to changes in patterns. It has two constructors, one is
 * used when saving a playlist from channels in the mixer to a JsonArray
 * and the other is used when loading a playlist through a JsonArray
 * from a .json file
 * @author Moses Swai
 */
public class Playlist{
    JsonArray playlistArray;

    /**
     * Constructor:
     * Use this when saving a playlist from channels in the mixer.
     * This constructor creates a JsonObject from the clip in each
     * of the channels, including the master channel. It then modifies
     * the JsonObject by saving the pattern label for pattern events
     * and parameter events that have a pattern as the parent
     * @param lx LX object
     */
    Playlist (LX lx) {
        this.playlistArray =  new JsonArray();

        // First save the master channel clip
        LXClip masterClip = lx.engine.mixer.masterBus.getClip(0);
        JsonObject masterClipObj = new JsonObject();
        masterClip.save(lx, masterClipObj);
        playlistArray.add(masterClipObj);

        // Then save all the channel clips
        for (LXAbstractChannel abstractChannel : lx.engine.mixer.channels) {
            LXChannel channel = (LXChannel)abstractChannel;
            LXClip clip = channel.getClip(0);
            JsonObject clipObj = new JsonObject();
            clip.save(lx, clipObj);
            createPlaylist(lx, clipObj, channel);
            playlistArray.add(clipObj);
        }
    }

    /**
     * Constructor:
     * Use this when loading a playlist through a JsonArray from a file.
     * The expectation is that the JsonObjects in the JsonArray were
     * already modified to include the pattern labels when the JsonArray
     * was being created.
     * @param array JsonArray from a playlist .json file
     */
    Playlist (JsonArray array) {
        this.playlistArray = array;
    }

    /**
     * Returns a modified JsonArray of the playlist. USE THIS AFTER
     * CONSTRUCTING THE PLAYLIST CLASS THROUGH LX CHANNELS
     * @return JsonArray of a modified playlist
     */
    public JsonArray getPlaylistArray(){
        return this.playlistArray;
    }


    /**
     * Loads a playlist JsonArray onto the LX channels. USE THIS AFTER
     * CONSTRUCTING THE PLAYLIST CLASS THROUGH A JsonArray
     * The function takes in an LX object and iterates through the channels
     * to load respective clips. Before each load, it calls createClip to
     * modify the clip's JsonObject in case the pattern order has change
     * @param lx LX object
     */
    public void loadPlaylist(LX lx) {
        // First load the master channel
        LXClip masterClip = lx.engine.mixer.masterBus.getClip(0);
        JsonObject masterClipObj = playlistArray.get(0).getAsJsonObject();
        try {
            masterClip.load(lx, masterClipObj);
        } catch (Exception ex) {
            System.err.println("Error loading master channel clip");
        }

        // Then load the the channels
        List<LXAbstractChannel> channels = lx.engine.mixer.channels;
        for (int i = 1; i < playlistArray.size(); i++) {
            LXChannel channel = (LXChannel)channels.get(i-1);
            LXClip clip = channel.getClip(0);
            JsonObject clipObj = playlistArray.get(i).getAsJsonObject();
            createClip(lx, clipObj, channel);
            try {
                clip.load(lx, clipObj);
            } catch (Exception ex) {
                System.err.println("Error loading channel " + i + " clip");
            }
        }
    }


    /*
    Modifies the JsonObject for each clip in the channel. The function adds
    a patternLabel property to all the pattern events. It also replaces the
    pattern index on the path of all parameter events that have a pattern as
    the parent with the pattern label
     */
    private void createPlaylist(LX lx, JsonObject obj, LXChannel channel) {
        JsonArray parameterLanes = obj.getAsJsonArray("parameterLanes");
        for (JsonElement laneElement: parameterLanes) {
            JsonObject lane = laneElement.getAsJsonObject();
            String laneType = lane.get("laneType").getAsString();
            if (laneType.equals("pattern")) {
                JsonArray patternEvents = lane.get("events").getAsJsonArray();
                for (JsonElement event : patternEvents) {
                    LXPattern pattern = channel.getPattern(event.getAsJsonObject().get("patternIndex").getAsInt());
                    event.getAsJsonObject().addProperty("patternLabel", pattern.getLabel());
                }
            } else if (laneType.equals("parameter")) {
                String path = lane.get("path").getAsString();
                if (path.startsWith("/pattern")) {
                    String patternIndex = path.substring(path.indexOf('/',1)+1, path.lastIndexOf('/'));
                    LXPattern pattern = channel.getPattern(Integer.parseInt(patternIndex)-1);
                    path = path.replaceFirst(patternIndex, pattern.getLabel());
                    lane.addProperty("path", path);
                }
            }
        }
    }


    /*
    Restores the JsonObject for each clip in the channel. The function enforces
    the pattern index on all pattern events to resemble the current order of
    patterns in the channel. It also replaces the pattern label on the path of
    all parameter events that have a pattern as the parent with the pattern index
     */
    private void createClip (LX lx, JsonObject obj, LXChannel channel) {
        JsonArray parameterLanes = obj.getAsJsonArray("parameterLanes");
        for (JsonElement laneElement: parameterLanes) {
            JsonObject lane = laneElement.getAsJsonObject();
            String laneType = lane.get("laneType").getAsString();
            if (laneType.equals("pattern")) {
                JsonArray patternEvents = lane.get("events").getAsJsonArray();
                for (JsonElement event : patternEvents) {
                    String patternLabel = event.getAsJsonObject().get("patternLabel").getAsString();
                    try {
                        LXPattern pattern = channel.getPattern(patternLabel);
                        event.getAsJsonObject().addProperty("patternIndex", pattern.getIndex());
                    } catch(NullPointerException ex) {
                        System.err.println("Could not find pattern with name " + patternLabel);
                    }
                }
            } else if (laneType.equals("parameter")) {
                String path = lane.get("path").getAsString();
                if (path.startsWith("/pattern")) {
                    String patternLabel = path.substring(path.indexOf('/',1)+1, path.lastIndexOf('/'));
                    try{
                        LXPattern pattern = channel.getPattern(patternLabel);
                        path = path.replaceFirst(patternLabel, String.valueOf(pattern.getIndex()+1));
                        lane.addProperty("path", path);
                        lane.addProperty("componentID", pattern.getId());
                    } catch(NullPointerException ex) {
                        System.err.println("Could not find pattern with name " + patternLabel);
                    }

                }
            }
        }
    }

}
