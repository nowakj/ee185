package engine;
import heronarts.lx.mixer.LXChannel;
import model.FlightModel;
import model.WingModel;

import heronarts.lx.LX;
import heronarts.lx.pattern.LXModelPattern;
import heronarts.lx.model.LXPoint;
public abstract class FlightPattern extends LXModelPattern<FlightModel> {

  protected int[] skews = null;

  public FlightPattern(LX lx) {
    super(lx);
//    this.addLayer(new LXLayer(lx, new LXDeviceComponent(lx, "skew")));
    skews = ((FlightModel) lx.getModel()).getAllWingsSkew();
  }

  /**
   * Get the current angle for this wing
   *
   * @param wing WingModel
   * @return Current angle of this wing
   */
  public int getSkew(WingModel wing) {
    return this.skews[wing.getIndex()];
  }

  /**
   * Update the angle for the given wing
   *
   * @param wing WingModel
   * @param skew The angle to set for this point
   */
  public void setSkew(WingModel wing, int skew) {
    this.skews[wing.getIndex()] = skew;
  }


//  // REVIEW(mcslee): I would suggest collapsing your distinction between
//  // the interface and class, then these metohds will just work without
//  // an extra layer of method call abstraction around LXPoint, the above
//  // methods should be sufficient
//  public int getColor(LightSamplePoint p) {
//    return this.colors[p.getIndex()];
//  }
//
//  public void setColor(LightSamplePoint p, int color) {
//    this.colors[p.getIndex()] = color;
//  }

}
