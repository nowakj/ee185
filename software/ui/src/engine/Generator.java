package engine;
//package src;

// Generate commands for one flyer
// Need access to right and left rgb, body rgb and right and left angle values
// Take flyer instance as parameter

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import model.FlightModel;
import model.FlyerModel;
import model.Geometry;
import model.LightSamplePointModel;

import java.io.DataInput;
import heronarts.lx.LX;
import heronarts.lx.output.LXOutput;
import java.awt.Color;

/*
 * Generates python command for fractal flyer
 * 	TODO change colour representation in model from string to value
 */
public class Generator extends LXOutput implements Geometry {
  final FlightModel flight;
  final List<FlyerModel> flyers;
  final DataInput[] in;
  final PrintStream[] out;

  protected int[] frameColors;

  static final String LEFT_WING_POS = "wing_angle(LEFT, %s)\n";
  static final String RIGHT_WING_POS = "wing_angle(RIGHT, %s)\n";
  static final String LEFT_WING_LEDs = "wing_leds(LEFT, (%d, %d, %d), (%d, %d, %d), (%d, %d, %d))\n";
  static final String RIGHT_WING_LEDs = "wing_leds(RIGHT, (%d, %d, %d), (%d, %d, %d), (%d, %d, %d))\n";
  static final String BODY_LED = "body_leds((%d, %d, %d))";


  public Generator(LX lx, FlightModel flight, DataInput[] inSockets, PrintStream[] outSockets) {
    super(lx);
    this.flight = flight;
    this.flyers = this.flight.getFlyers();
    this.in = inSockets;
    this.out = outSockets;
  }

  private Color getRGBColor(LightSamplePointModel point) {
    return new Color(this.frameColors[point.getIndex()]);
  }

  private Color[] getRGBColors(List<LightSamplePointModel> points) {
    // REVIEW(mcslee) - unclear benefit to making dynamic copies with java.awt.Color on every frame...
    // consider using int[] and you have the LXColor.red()/LXColor.green()/LXColor.blue() methods
    // which are simlar to java.awt.Color.getRed(), etc.
    //
    // There's no fundamental problem with what you're doing here, but you're just going to be putting
    // the garbage collector to work
	  Color[] ledColors = new Color[points.size()];
	  for (int i = 0; i < points.size(); i++) {
	    ledColors[i] = new Color(this.frameColors[points.get(i).getIndex()]);
	  }
	  return ledColors;
  }

  /*
   * ## Python API

		For a Python API, I’d suggest these three functions:

		wing_leds(LEFT|RIGHT, (r, g, b), (r, g, b,), (r, g, b,))
		body_leds((r, g, b)
		wing_angle(LEFT|RIGHT, angle)
   */

  /*
   * TODO handle the case where only one parameter has changed
   * Perhaps that should be handled by engine
   */

  private ArrayList<String> generateCommands(int flyerIndex) {
    /*
     * String s = "hello %s!";
     * s = String.format(s, "world");
     * assertEquals(s, "hello world!");
     */
	int leftWingPos = flight.getFlyerLeftWingSkew(flyerIndex);
	int rightWingPos = flight.getFlyerRightWingSkew(flyerIndex);

	LightSamplePointModel bodyLightPoint = flight.getFlyerBodyLight(flyerIndex);


	List<LightSamplePointModel> leftLEDs = flight.getFlyerLeftWingLights(flyerIndex);
	List<LightSamplePointModel> rightLEDs = flight.getFlyerRightWingLights(flyerIndex);

    ArrayList<String> commands = new ArrayList<String>();

    Color[] leftLights = getRGBColors(leftLEDs);
    Color[] rightLights = getRGBColors(rightLEDs);
    Color bodyLED = getRGBColor(bodyLightPoint);


    commands.add(String.format(LEFT_WING_POS, leftWingPos));
    commands.add(String.format(RIGHT_WING_POS, rightWingPos));

    commands.add(String.format(BODY_LED,
    		bodyLED.getRed(), bodyLED.getGreen(), bodyLED.getBlue()));

    commands.add(String.format(LEFT_WING_LEDs,
    		leftLights[0].getRed(), leftLights[0].getGreen(), leftLights[0].getBlue(),
    		leftLights[1].getRed(), leftLights[1].getGreen(), leftLights[1].getBlue(),
    		leftLights[2].getRed(), leftLights[2].getGreen(), leftLights[2].getBlue())
    		);

    commands.add(String.format(RIGHT_WING_LEDs,
    		rightLights[0].getRed(), rightLights[0].getGreen(), rightLights[0].getBlue(),
    		rightLights[1].getRed(), rightLights[1].getGreen(), rightLights[1].getBlue(),
    		rightLights[2].getRed(), rightLights[2].getGreen(), rightLights[2].getBlue())
    		);

    return commands;
  }

  @Override
  protected void onSend(int[] colors, double brightness) {
//    super.onSend(colors, brightness);


    // REVIEW(mcslee): here we are being given the engine's composited and mixed
    // colors for this frame
    this.frameColors = colors;

    for (int i=0; i<flyers.size(); i++) {
      ArrayList<String> commands = generateCommands(i);
      for (String command : commands) {
        try {
            //          out[i].println(command);
        } catch (Exception e) {
          System.out.println("failed to write command!");
        }
      }
    }
  }

  @Override
  protected void onSend(int[] arg0, byte[] arg1) {
    throw new UnsupportedOperationException("LXDatagramOutput does not implement onSend by glut");
  }

}
