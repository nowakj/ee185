package engine;
import com.google.gson.JsonArray;

import model.FlightModel;
import model.FlyerConfig;

import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import heronarts.lx.LX;
import heronarts.lx.mixer.*;
import heronarts.lx.studio.LXStudio;
import heronarts.lx.pattern.LXPattern;
import heronarts.lx.clip.LXClip;

/**
 * Central controller for a FLIGHT installation, responsible for
 * loading and running playlists, loading the available Patterns,
 * and configuring output to generate Python code.
 *
 * @author Philip Levis <pal@cs.stanford.edu>
 * @author Moses Swai
 * @author Ashley Chen
 */

public abstract class FlightEngine implements FlightPlaylist {

  static final int NUM_CHANNELS = 8;

  // Directory root from which to read/write files
  final private String projectPath;

  // Helper class for reading/write data
  final private IO io;

  // The ground-truth copy of the data model: Flyer locations, etc.
  final private FlightModel model;

  // The LXStudio used to drive the whole application.
  final private LXStudio lx;

  // Because we need to be able to access channels quite often, just
  // store references to them here. These are also passed into the
  // LXStudio LXMixer so it mixes and displays them properly.
  final private List<LXChannel> channels;

  // The set of Patterns that can be selected for each channel.
  // We have this here so we can query this (e.g., size) without
  // having to poke at a channel.
  final private ArrayList<LXPattern> patterns;

  // Helper class that generates Python code to control FLIGHT based
  // on the state of the FlightModel.
  private Generator generator;

  public FlightEngine(String projectPath, FlightModel model, LXStudio lx) {
    this.projectPath = projectPath;
    this.io = new IO(projectPath);
    this.model = model;
    this.lx = lx;
    this.patterns = new ArrayList<LXPattern>();
    this.channels = new ArrayList<LXChannel>();
    
    /* All engine configuration */
    configureGeneratorOutput();
    configureChannels();

  }

  /***** PUBLIC FUNCTIONS *****/

  /**
   * Returns the FlightModel used to create the LXModel
   *
   * @return The FlightModel
   */
  public FlightModel getModel() {
      return model;
  }


  /**
   * Returns an array with all the flyer configurations
   * This array can be used to save the current flyer states
   * to a .json file using the IO class
   *
   * @return A FlyerConfig array
   */
  public FlyerConfig[] getFlyerConfigs() {
      return model.getFlyerConfigs();
  }

  /**
   * Returns the number of patterns in each channel.
   * This assumes that every channel has the same patterns
   *
   * @return The number of patterns
   */
  public int numPatterns() {
      return this.patterns.size();
  }

  /**
   * Returns the number of channels in the LXMixerEngine
   * NOTE: This does not count the master channel
   *
   * @return The number of channels
   */
  public int numChannels() {
      return this.lx.engine.mixer.channels.size();
  }

  /**
   * Returns the LXMixerEngine object in the LXEngine
   *
   * @return A list of LXAbstractChannel
   */
  public LXMixerEngine getMixerEngine() {
    return lx.engine.mixer;
  }

  /**
   * Returns the loaded channels in the LX engine.
   * NOTE: This does not include the master channel
   *
   * @return A list of LXAbstractChannel
   */
  List<LXAbstractChannel> getChannels() {
    return lx.engine.mixer.channels;
  }

  /**
   * Returns the IO class object for saving and loading
   * flyer configurations and playlist
   *
   * @return The IO class object
   */
  public IO getIO(){
    return io;
  }

  /**
   * Sets up and starts recording a playlist:
   * Arms all the channels in the mixer to a recording state and
   * triggers all the clips at index 0 (each channel is configured
   * to have one clip only
   */
  public void startRecording() {
    lx.engine.mixer.masterBus.arm.setValue(true);
    for (LXAbstractChannel channel : getChannels()) {
      channel.arm.setValue(true);
    }
    getMixerEngine().launchScene(0);
  }

  /**
   * Stops recording a playlist.
   */
  public void stopRecording() {
    lx.engine.mixer.masterBus.arm.setValue(false);
    for (LXAbstractChannel channel : getChannels()) {
      channel.arm.setValue(false);
    }
  }


  /**
   * Starts playback of a loaded playlist:
   * Disarms all the channels in the mixer to a playback state
   * and triggers all the clips at index 0 (each channel is
   * configured to have one clip only
   */
  public void playbackPlaylist() {
    lx.engine.mixer.masterBus.arm.setValue(false);
    for (LXAbstractChannel channel : getChannels()) {
      channel.arm.setValue(false);
    }
    getMixerEngine().launchScene(0);
  }

  /**
   * Stops recording or playback of a playlist:
   * Stops all clips in the mixer. This will stop recording if
   * the channels were in a recording state or stop playback if
   * the channels were in a playback state
   */
  public void stopPlaylist() {
    getMixerEngine().stopClips();
  }

  /**
   * Loads a playlist from a file:
   * Creates a Playlist object from a JsonArray that is loaded
   * by the IO class. It then calls the Playlist class function
   * that loads the playlist array onto the channels
   * @param filename The relative path of the playlist file to be loaded
   */
  public void loadPlaylist(String filename){
      try {
          JsonArray file = io.loadPlaylistFile(filename);
          Playlist playlist = new Playlist(file);
          playlist.loadPlaylist(lx);
      }
      catch (Exception e) {
          System.err.println("Error loading playlist from " + filename);
          e.printStackTrace();
      }
  }

  /**
   * Loads a playlist from a file:
   * Creates a Playlist object from a JsonArray that is loaded
   * by the IO class. It then calls the Playlist class function
   * that loads the playlist array onto the channels
   * @param file The file object of the playlist file to be loaded
   */
  public void loadPlaylist(File file){
    Playlist playlist = new Playlist(io.loadPlaylistFile(file));
    playlist.loadPlaylist(lx);
  }

  /**
   * Saves a loaded or recorded playlist onto a file:
   * Creates a Playlist object from a the channels in the mixer
   * and then save the resulting JsonArray through the IO class
   * @param filename The relative path of the playlist file to be saved
   */
  public void savePlaylist(String filename){
    Playlist playlist = new Playlist(lx);
    io.savePlaylistFile(playlist.getPlaylistArray(), filename);
  }

  /**
   * Saves a loaded or recorded playlist onto a file:
   * Creates a Playlist object from a the channels in the mixer
   * and then save the resulting JsonArray through the IO class
   * @param file The file object of the playlist file to be saved
   */
  public void savePlaylist(File file){
    Playlist playlist = new Playlist(lx);
    io.savePlaylistFile(playlist.getPlaylistArray(), file);
  }


  /**
   * Sets whether the current playlist should run once or loop.
   */
  public void setPlaylistLooping(boolean looping) {
    for (LXChannel channel: this.channels) {
      LXClip clip = channel.getClip(0);
      clip.loop.setValue(looping);
    }
  }

  // PRIVATE FUNCTIONS

  /* Adds patterns to the pattern list passed as parameter.
   * This pattern is useful because each channel has its own
   * list of patterns, since each instance of a pattern might
   * have different settings.
   */
  private void addPatterns(ArrayList<LXPattern> patterns) {
    patterns.add(new ColorSweep(lx));
    patterns.add(new ColorWall(lx));
    patterns.add(new ColorWave(lx));
    patterns.add(new ColorWheel(lx));
    patterns.add(new RaspberryTrip(lx));
  }
 
  /* Create an array with a new instance of each available Pattern. */
  private LXPattern[] createPatternArray() {
    ArrayList<LXPattern> list = new ArrayList<LXPattern>();
    addPatterns(list);
    return list.toArray(new LXPattern[patterns.size()]);
  }
    

  /* Creates all of the channels for the mixer, giving each its
   * own copy of each available pattern and giving it a clip for
   *  later recording or playback.
   */
  private void configureChannels() {
    addPatterns(this.patterns);
    LXMixerEngine mixer = lx.engine.mixer;
    mixer.removeChannel(mixer.getLastChannel());
    mixer.clear();
    for (int c = 0; c < NUM_CHANNELS; ++c) {
      LXChannel channel = mixer.addChannel(c, createPatternArray());
      channels.add(channel);
      // By default channel 1 is on and others are off.
      if (c == 0) { 
        channel.fader.setValue(1);
      } else {
        channel.fader.setValue(0);
      }
      channel.addClip();
    }
    mixer.setFocusedChannel(mixer.getChannel(0));
    mixer.selectChannel(mixer.getChannel(0));
    mixer.masterBus.addClip();
  }
    
  /* Configures the GeneratorOutput */
  private void configureGeneratorOutput() {
    PrintStream[] outputSockets = new PrintStream[76];
    Arrays.fill(outputSockets, System.out);

    try {
      generator = new Generator(lx, model, null, outputSockets);
      lx.addOutput(generator);
    } catch (Exception ex) {
        System.out.println("IN generator");
      System.out.println(ex);
    }
  }

}


