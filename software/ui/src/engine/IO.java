package engine;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.reflect.Type;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import model.FlyerConfig;

/**
 * IO class contains all the functions that are used for reading and writing
 * files by the UI. This involves loading and saving of flyer configurations
 * and playlists. All files are saved in .json format. The path for all files
 * should be relative to the project path passed when constructing the object
 * @author Moses Swai
 */
public class IO {

  final String projectPath;

  /**
   * CONSTRUCTOR
   * @param projectPath The top level project path from which the UI is run
   */
  public IO(String projectPath) {
    this.projectPath = projectPath;
  }

  /***** PUBLIC FUNCTIONS *****/

  /**
   * Loads a flyer configuration from a JSON file
   * @param filename The name of the configuration file relative to the project path
   * @return A FlyerConfig array
   */
  public FlyerConfig[] loadConfigFile(String filename) {
    return loadJSONFile(createPath(filename), new TypeToken<FlyerConfig[]>() {}.getType());
  }

  /**
   * Saves the current flyer configuration to a JSON file
   * @param config A FlyerConfig array
   * @param filename The name of the configuration file to be saved relative to the project path
   */
  public void saveConfigFile(FlyerConfig[] config, String filename) {
    saveJSONToFile(config, createPath(filename));
  }

  /**
   * Loads a saved FLIGHT playlist from a JSON file
   * @param filename The name of the playlist file relative to the project path
   * @return A JsonArray object with the playlist
   */
  public JsonArray loadPlaylistFile(String filename) {
    return loadJSONFile(createPath(filename), JsonArray.class);
  }

  /**
   * Loads a saved FLIGHT playlist from a JSON file
   * @param file A File object specifying the playlist file within the project path
   * @return A JsonArray object with the playlist
   */
  public JsonArray loadPlaylistFile(File file) {
    return loadJSONFile(file.getPath(), JsonArray.class);
  }

  /**
   * Saves a FLIGHT playlist set to a JSON file
   * @param array A JsonArray with the playlist
   * @param filename The name of the playlist file to be saved relative to the project path
   */
  public void savePlaylistFile(JsonArray array, String filename) {
    saveJSONToFile(array, createPath(filename));
  }

  /**
   * Saves a FLIGHT playlist to a JSON file
   * @param array A JsonArray with the playlist
   * @param file A file object specifying the playlist file to be saved within the project path
   */
  public void savePlaylistFile(JsonArray array, File file) {
    saveJSONToFile(array, file.getPath());
  }

  /**
   * Loads a saved output file that contains all the file names for generator outputs from a JSON fiel
   * @param filename The name of the output file relative to the project path
   * @return A String array
   */
  public String[] loadOutputFile(String filename) {
    return loadJSONFile(createPath(filename), String[].class);
  }

  /**
   * Saves the generator output names to a JSON file
   * @param array A String array with the generator output names
   * @param filename The name of th output file to be saved relative to the project path
   * */
  public void saveOutputFile(String[] array, String filename) {
    saveJSONToFile(array, createPath(filename));
  }

  /***** PRIVATE FUNCTIONS *****/

  /* Concatenate file name to the project path */
  private String createPath(String filename) {
    return projectPath + File.separator + filename;
  }

  /* Loads a JSON File from the specified 'path' and into the given type 'typeToken' */
  private static <T> T loadJSONFile(String path, Type typeToken) {
    try (Reader reader = new BufferedReader(new FileReader(path))) {
      return new Gson().fromJson(reader, typeToken);
    } catch (IOException ioe) {
      System.out.print("Error reading json file: ");
      System.out.println(ioe.getMessage());
    }
    return null;
  }

  /* Serialize and save flyerConfig list 'config' to Json */
  private static void saveJSONToFile(FlyerConfig[] config, String path) {
    try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
      writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
    } catch (IOException ioe) {
      System.err.println("Error writing List to json file.");
    }
  }

  /* Serialize and save a playlist 'array' to Json */
  private static void saveJSONToFile(JsonArray array, String path) {
    try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
      writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(array));
    } catch (IOException ioe) {
      System.err.println("Error writing JsonArray to json file.");
    }
  }

  /* Serialize and save an output names 'array' to Json */
  private static void saveJSONToFile(String[] array, String path) {
    try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(path)))) {
      writer.write(new GsonBuilder().setPrettyPrinting().create().toJson(array));
    } catch (IOException ioe) {
      System.err.println("Error writing JsonArray to json file.");
    }
  }
}