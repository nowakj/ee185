package engine;
import java.util.List;

import model.FlyerModel;
import model.Geometry;
import model.LightSamplePointModel;
import model.WingModel;

import heronarts.lx.LX;
import heronarts.lx.modulator.SawLFO;
import heronarts.lx.modulator.SinLFO;
import heronarts.lx.modulator.TriangleLFO;
import heronarts.lx.parameter.BoundedParameter;

/**
 * Rotates all the Flyers through colors on the HSB color wheel,
 * controlled by a sinusoid. The wing position is based on the hue,
 * with 0 being fully down and 360 being fully up.  There is one
 * control, the speed they move through the wheel.
 *
 * @author Philip Levis <pal@cs.stanford.edu>
 */
class ColorWheel extends FlightPattern {
    final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
    final SinLFO color = new SinLFO(0, 360, period);


    ColorWheel(LX lx) {
        super(lx);
        addParameter("ColorWheel_PERIOD", period);
        addModulator(color).start();
    }

    @Override
    public void run(double deltaMs) {
        float hue = color.getValuef();
        int skew = (int)((hue - 180f) / 2);

        for (WingModel wing: model.getAllWings()) {
            setSkew(wing, skew);
        }
        for (LightSamplePointModel bodyPoint : model.getAllBodyLights()) {
            setColor(bodyPoint, LX.hsb(hue, 100, 100));
        }
        for (LightSamplePointModel wingPoint : model.getAllWingsLights()) {
            setColor(wingPoint, LX.hsb(hue, 100, 100));
        }
    }
}

/**
 * This pattern flaps all the wings in unison, while a pink streak
 * goes from the first flyer to the last, leaving a graduated pink
 * trail.  Flyers not included in the streak are cycling through
 * pastel colors
 * @author achen
 *
 */
class RaspberryTrip extends FlightPattern {
  int NUM_FLYERS = Geometry.NUM_FLYERS;
  int FLYER_MOD = NUM_FLYERS - 1;
    

  final BoundedParameter rate = new BoundedParameter("RATE", 5000, 5000, 100000);
  final SawLFO flyerIndex = new SawLFO(0, FLYER_MOD, rate);

  final BoundedParameter speed = new BoundedParameter("SPEED", 5000, 12500, 25000); 
  final SinLFO colorFlap = new SinLFO(0, 360, speed);

  RaspberryTrip(LX lx) {
    super(lx);
    addParameter("RaspberryTrip_RATE", rate);
    addModulator(flyerIndex).start();
    addParameter("RaspberryTrip_SPEED", speed);
    addModulator(colorFlap).start();
  }

  private int calculateHSB(int highlightedFlyerIndex, int flyerIndex, float hue) {
    int pinkHue = 330;
    int pinkSat = 100;
    int pinkBri = 100;
    int diff = highlightedFlyerIndex - flyerIndex;
    if (diff < 0) {
      diff += NUM_FLYERS;
    }

    if (diff == 0) {
      return LX.hsb(pinkHue, pinkSat - 5, pinkBri - 5);
    } else if (diff <= 3) {
      return LX.hsb(pinkHue, pinkSat - (10 * diff), pinkBri - (10 * diff));
    } else if (diff <= 11) {
      return LX.hsb(pinkHue, pinkSat - 30 - diff, pinkBri - 30);
    } else if (diff <= 33) {
      return LX.hsb(pinkHue, pinkSat - 31 - (2 * diff), pinkBri - 30 - diff/5);
    } else if (diff == 34) {
      return LX.hsb(0, 1, 60);
    } else {
      return LX.hsb(hue, 25, 60);
    }
  }

  @Override
  public void run(double deltaMs) {
    List<LightSamplePointModel> allLights = model.getAllLights();
    int highlightedFlyerIndex = (int) flyerIndex.getValue();

    float hue = colorFlap.getValuef();
    for (LightSamplePointModel lightPoint : allLights) {
      int flyerIndex = lightPoint.getBodyIndex();
      setColor(lightPoint, calculateHSB(highlightedFlyerIndex, flyerIndex, hue));
    }

    int skew = (int)((hue - 180f) / 2);
    for (WingModel wing : model.getAllWings()) {
      setSkew(wing, skew);
    }

  }
}


/**
 * Makes a color wave across FLIGHT, such that full range of colors
 * (hue in HSB) is shown across plane of the front window. There are
 * four controls: the speed of the wave, the polar direction of the wave (0
 * degrees is bottom to top), the saturation and the brightness. The wing
 * position is set to the color hue, with red (0) being fully down and
 * blue (360 being up).
 *
 * @author Philip Levis <pal@cs.stanford.edu>
 */
class ColorWave extends FlightPattern {

    final BoundedParameter period = new BoundedParameter("PERIOD", 15000, 1000, 60000);
    final TriangleLFO position = new TriangleLFO(0, 1, period);
    final BoundedParameter angle = new BoundedParameter("ANGLE", 0, 360, 0);
    final BoundedParameter saturation = new BoundedParameter("SATURATION", 100, 0, 100);
    final BoundedParameter brightness = new BoundedParameter("BRIGHTNESS", 100, 0, 100);
    ColorWave(LX lx) {
        super(lx);
        addParameter("ColorWave_PERIOD", period);
        addParameter("ColorWave_ANGLE", angle);
        addParameter("ColorWave_SATURATION", saturation);
        addParameter("ColorWave_BRIGHTNESS", brightness);
        addModulator(position).start();
    }

    @Override
    public void run(double deltaMs) {
        float s = saturation.getValuef();
        float b = brightness.getValuef();
        
        float opposite = (float)Math.tan(angle.getValuef() * Math.PI / 180f) * Geometry.FRONT_WINDOW_NADIR_HEIGHT;
        float adjacent = Geometry.FRONT_WINDOW_NADIR_HEIGHT;
        float hypotenuse = (float)Math.sqrt(opposite * opposite + adjacent * adjacent);
        float height = hypotenuse;
        int center = (int)(position.getValuef() * height);

        // Make the height of the window a color gradient from 0-360,
        // with the position of the LFO being the zero point: the
        // gradient goes up the window and wraps around.
        for (FlyerModel flyer: model.getFlyers()) {
            float y = flyer.getY() * (float)Math.cos(angle.getValuef());
            float x = flyer.getX() * (float)Math.sin(angle.getValuef());
            float distance = (float)Math.sqrt((x * x) + (y * y));
            distance += height;
            float offset = (distance - center) % height;
            float hue = 360f * offset / height;
            for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
                setColor(point, LX.hsb(hue, s, b));
            }
            int skew = (int)(-90f + 180f * position.getValuef());
            setSkew(flyer.getLeftWing(), skew);
            setSkew(flyer.getRightWing(), skew);
        }
    }
}

/**
 * Makes a color sweep across FLIGHT, starting from front facing apex of the
 * building going all the way to the back. There are three controls: the speed
 * of the sweep, the range of fliers alight up by the sweep and the hue of the
 * sweep. The sweep is tries to resemble a wave done by fans in sports stadiums.
 * The wing are set to open up as the sweep goes outwards and close as it contracts
 * back to the apex.
 * @author Moses Swai
 */
class ColorSweep extends FlightPattern {

    float max_x = Geometry.FRONT_WINDOW_WIDTH;
    float max_z = Geometry.Z_ROOM;
    float max_distance = (float)Math.sqrt((max_x * max_x) + (max_z * max_z));

    final BoundedParameter period = new BoundedParameter("PERIOD", 10000, 1000, 50000);
    final BoundedParameter range = new BoundedParameter("RANGE", 5, 0, 100);
    final BoundedParameter hue = new BoundedParameter("HUE", 100, 0, 360);
    final TriangleLFO position = new TriangleLFO(0, max_distance, period);

    float prev_position = position.getValuef();

    ColorSweep(LX lx) {
        super(lx);
        addParameter("ColorSweep_PERIOD", period);
        addParameter("ColorSweep_RANGE", range);
        addParameter("ColorSweep_HUE", hue);
        addModulator(position).start();
    }

    @Override
    public void run(double deltaMs) {

        for (FlyerModel flyer: model.getFlyers()) {
            float x = flyer.getX();
            float z = flyer.getZ();
            float distance = (float)Math.sqrt((x * x) + (z * z));

            if (position.getValuef() < distance+range.getValuef() && position.getValuef() > distance-range.getValuef() ) {
                for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
                    setColor(point, LX.hsb(hue.getValuef(), 100, 100));
                }
                int skew;
                if (prev_position < position.getValuef()) {
                    skew = -60;
                } else {
                    skew = 60;
                }
                setSkew(flyer.getLeftWing(), skew);
                setSkew(flyer.getRightWing(), skew);
            } else {
                for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
                    setColor(point, LX.hsb(0, 0, 0));
                }
            }
        }
        prev_position = position.getValuef();
    }
}

/**
 * Similar to ColorWave except it propogates a single color. There are
 * four controls: the speed of the wave, the polar direction of the wave (0
 * degrees is bottom to top), the color and the length of the wave.  The
 * wing saturation is set to 100 and the brightness is dictated by the a
 * combination of the position and length of the wave.
 *
 * @author Andrew Malty <maltyam@stanford.edu>
 */
class ColorWall extends FlightPattern {

    final BoundedParameter speed = new BoundedParameter("PERIOD", 10000, 20000, 1000);
    final SawLFO position = new SawLFO(0, 1, speed);
    final BoundedParameter angle = new BoundedParameter("ANGLE", 0, 360, 0);
    final BoundedParameter color = new BoundedParameter("COLOR",0, 360, 50);
    final BoundedParameter length = new BoundedParameter("LENGTH",1, 50, 1);
    ColorWall(LX lx) {
        super(lx);
        addParameter("ColorWall_SPEED", speed);
        addParameter("ColorWall_ANGLE", angle);
        addParameter("ColorWall_COLOR", color);
        addParameter("ColorWall_LENGTH", length);
        addModulator(position).start();
    }

    @Override
    public void run(double deltaMs) {
        float s = 100;
        float hue = color.getValuef();
        float l = length.getValuef();
        
        float opposite = (float)Math.tan(angle.getValuef() * Math.PI / 180f) * Geometry.FRONT_WINDOW_NADIR_HEIGHT;
        float adjacent = Geometry.FRONT_WINDOW_NADIR_HEIGHT;
        float hypotenuse = (float)Math.sqrt(opposite * opposite + adjacent * adjacent);
        float height = hypotenuse;
        int center = (int)(position.getValuef() * height);

        for (FlyerModel flyer: model.getFlyers()) {
            float y = flyer.getY() * (float)Math.cos(angle.getValuef());
            float x = flyer.getX() * (float)Math.sin(angle.getValuef());
            float distance = (float)Math.sqrt((x * x) + (y * y));
            distance += height;
            float offset = (distance - center) % height;
            float b = 100f * (float)Math.pow(offset / height,l);
            for (LightSamplePointModel point : model.getFlyerLights(flyer.getIndex())) {
                setColor(point, LX.hsb(hue, s, b));
            }
        }
    }
}

