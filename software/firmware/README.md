# FLIGHT: Packard Interactive Light Sculpture Project

This directory stores source code for the firmware that runs on a Fractal Flyer.
A FractalFlyer has a [Stanford Maxwell](https://github.com/maholli/sam32) board, 
which is a variant of the [Feather M4 Express
board](https://www.adafruit.com/product/3857). The firmware provides a [CircuitPython](https://circuitpython.org/)-based programming interface.

