# Assignment 4: Finishing the Foundation

*Written by Philip Levis*

**Due: Tuesday, May 19, 2020 at 2:30 PM (before class)**

## Goals

When you're done with this assignment, you should
  - have a complete end-to-end system that reads in the state of FLIGHT in the UI and sends that to real Fractal Flyers or the simulator and a test that checks it's working,
  - have a completed Python environment and management of per-simulated flyer Python processes,
  - have a completely defined Java data model in LX studio,
  - have two prototype Effect implementations that use the Java data model,
  - have a compeltely defined geometric configuration file format, read both by the simulator and Engine,
  - have a design for how to import the DXF model of the stairwell into OpenGL,
  - have documented the software architecture and major APIs.

This seems like a lot! But it's really a collection of smaller tasks that together are
the important foundations for fleshing everything else out in the next few weeks.

I've attached names to each task. This means that you are responsible for that
task being completed. It doesn't mean you have to do it all by yourself -- if it's
much harder than it seems, feel free to ask for help from others. Or, if you want,
swap work around. But you'll be responsible for, next class, reporting out on
completion and questions that came up.

## End-to-end Test (Martin)

Now that we have the Engine generating Python scripts that are written
to a input/output for a Fractal Flyer (simulated or real), we can
set up a full end-to-end test and use it to check that everything is working.

This first version of the test should be a shell script that
  - creates a single Fractal Flyer in simulation,
  - invokes the Engine to connect to this Fractal Flyer using the pipe it creates,
  - has the Engine run a simple loop of changes that cause LXStudio to send updates, and
  - checks the output of the simulator to make sure it is correct.

To check the output, one simple way is to save the output once into a
file, then when you run the simulation in the test, put its output to 
a file, and run `diff` on the two files.

Note this means we will need to finalize and be rigid on what the
simulator outputs! In the long term, we might want a command-line option 
that tells the simulator to output data in this particular format
for testing.

## Python Environment and Intepreter Management (Samsara and Andrew)

The simulator should be able to:
  - start up an arbitrary number of Python interpreter processes,
  - create named pipes and directories based on a configuration
    file or command-line parameters,
  - have these Python interpreters communicate with the simulator
    process itself,
  - receive and process messages for Python library calls and
    change internal simulator state based on them,
  - properly clean up processes, named pipes, and directories
    when the simulator exits or crashes.
 
## Have a completely defined Java data model in LX studio (Ashley)

Based on feedback in class, finalize the Java data model so programs
will be able to easily iterate across the Flyers and act on a per-Flyer
basis. This will dovetail with the next item...

## Have two prototype Effect implementations that use the model (Ashley)

The first Effect should be like the Fade in Squared: it causes all of
the Flyers to change color and move their wings in a shared pattern.

The second Effect should alternate between the Flyers, setting one of
them white and raising its wings, while all the others are are set balack
and have lowered wings. The Flyer should change every 10 seconds or so.
This second Effect might be a good default one or one to run on for the 
end-to-end test.

## Have a completely defined geometric configuration file format, read both by the simulator and Engine (Andrew, Moses)

This involves deciding how the geometry of the Flyers is specified. I'd
like to suggest that we go with the idea of having 4 faces:
  1. The front window,
  2. The spiral exterior edge of the stairwell,
  3. The spiral interior edge of the stairwell, and
  4. The ceiling.

The front window allows arbitrary x/y positions in the window.

The edges have two values: the position along the spiral
(where bottom is 0 and top is X) and the hang distance from this
point in the strars.

The ceiling has 3 values: the pipe (one of 5? 7? check the model),
where on the pipe, and how far down from the pipe.

Also, each flyer should have 4 metadata values associated with it:
  - Name: the text on the wing,
  - Type: faculty, student, alumnus/alumna, project, organization,
  - Department: mostly for students, and
  - Year: a year associated with this particular bit of history.

The file format needs to be parsed by the simulator and Engine. It is
not necessary that the geometric properties (e.g., face: Front window,
x=6, y=20) is turned into X, Y, Z coordinates. We will do this later once
we have been able input the model and all of its reference points. So
for now when you read in the file, you should parse the geometry but
can just set x,y,z to be the index of the flyer.

## Have and test a plan for how to import the DXF model of the stairwell into OpenGL (Samsara)

Look into the DXF format and propose a way that the simulator will be
able to read it in. This could be we'll parse it ourselves, or use
an existing library. Test that the proposed approach will work (e.g.,
the library can parse the DXF file). We'll later then pull apart the
elements in the DXF and turn them into 3D geometries for drawing.


## Have documented the software architecture and major APIs (Everyone)

Each of you should spend 30-45 minutes documenting your software so far.
In the main README, include a high-level architectural diagram that ties
the particular elements to directories in `software`. Write Javadoc
comments or C comments for your APIs, especially those that other 
modules in the system use. Be detailed -- don't just say what it does, 
but exactly how it should behave, so someone writing to code that uses
the interface can know exactly how to use it. Update the per-directory
READMEs will information on what that directory contains and what
each file represents.


