# Assignment 2: Software Definition and Design

*Written by Philip Levis*

**Module Decomposition Due: Tuesday, April 21, 2020 at 2:30 PM (before class)**

**Module APIs Due: Monday, April 27 (to git repository)**

**Design Presentation: Tuesday, April 28, in class**

## Goals

When you're done with this assignment, you should

  - have an initial definition of what your part of your component does,
  - have worked with the other people in your component to decide on the boundaries between your part and theirs,
  - be ready to start prototyping your design.

## Components and Teams

We've divided the FLIGHT PC software into 3 major components:

  1. _Engine:_ the software that controls the state (wing position and LED
     colors) of each Fractal Flyer (FF) in FLIGHT and sends commands to
     FFs to put them in this state. The Engine is Java software that uses
     LX studio for managing the internal color state, effects, parameters,
     and mixing them. Other functionality includes the ability to save and 
     restore physical layouts of the piece as well as playlists of animations.
 
  2. _UI:_ the user interface (UI) that allows someone to visualize and 
     control the state of the Engine. This involves both the direct UI
     elements (layout, controls), the 3D visualization of the piece and
     the conceptual model presented to a user of how to control FLIGHT.

  3. _Simulator_: a separate, stand-alone program that will allow us to
     test the Engine and visualize FLIGHT at a greater fidelity than what
     the UI can provide. The simulator will read some of the same configuration
     files as the UI (e.g., the physical layout of FLIGHT), but will 
     appear to a Linux system exactly as FLIGHT will, as 76 USB flash
     drives and 76 USB serial ports. Using the simulator, people will
     be able to test and debug programming FLIGHT.

Here are the team assignments:

  1. _Engine:_: Ashley, Moses, Martin
  1. _UI_: Kathy, Kyle, Abdallah
  1. _Simulator_: Claire, Andrew, Samsara

### Team Responsibilities

Each component in the system is a reasonably complex piece of software.
To be successful, your team will need to carefully collaborate to 
figure out who is responsible for what parts of the component, and
adapt those responsibilities as you realize some parts are harder
and some are easlier than you initially guessed.

Each person on the team is responsible for a core piece of functionality.
If you think of your project as a geometric shape that defines everything
it does, each piece of functionality is a point in that shape. You need
to figure out how you are going to divide the shape starting at those points:
this involves covering a great deal of additional support code and
functionality than just the point that is at the center of your work.

Over the next weeks, you will scope out this division of responsiblity
in your group. An easy unit of granularity is a Java class or C file. 
Let's call each unit of software a *module*. Decompose your functionality
into modules and draw dependencies between then.
E.g., if the code in C file `X.c` needs to call functions
in `Y.c`, then X depends on Y.

Once you have this module and dependency graph, you want to assign modules
to group members. A good way to do this is by trying to assign modules
to people to minimize the dependencies between them. That is, assign
modules so that most dependencies, or dense sets of dependencies, are 
within a single person's part of the project. 

This is because the points of contact and
collaboration are the dependencies between people's parts. These are
the interfaces or APIs that you need to define early and well, and set
them in stone as well as you can. If one of your internal APIs change,
it's localized to your own code and you can adjust everything. If one of
the APIs someone else in your group uses has to change, you need to discuss
it with them and tradeoff between its impact to both of you.

## UI Decomposition

At a high level, the UI has three major components:

  1. A design of how someone describes and specifies what FLIGHT does.
     SQUARED had visual effects. FLIGHT is a bit different in that
     there are large-scale visual effects (when you look at the 
     entire pieces),  small-scale visual effects (what an individual
     Fractal Flyer looks like), and also the motion of the wings.
     Are these bound together into a single type of effect? Are they
     managed separately? How does the UI show someone the effects
     of their decisions at both the small and large scale?
     How does one adjust the physical layout (e.g., to see what
     a different arrangement might look like)?
  2. The design of the UI itself. What are the panels and controls?
     How are they laid out? 
  3. A visualization of how FLIGHT looks. This is Processing-based
     drawing code. Given that the wings and body may both have
     multiple sampling points, this will involve gradients and
     other color effects.

## Engine

At a high level, the Engine has three major components:

  1. The data model for storing the current state of FLIGHT. This
     involves deciding what data structures are used, how they are 
     linked, and how those abstractions can be used by the visualization
     and effect programming APIs.
  2. Saving and restoring the state of FLIGHT (its physical layout)
     as well as playlists. This involves also working with the simulation
     group so the physical layout file can be read by the simulator. 
  3. Generating Python commands for Fractal Flyers that control it to
     reflect the state in the engine (and therefore visualization). This
     also involves proposing what the Python API on each Fractal Flyer
     looks like.
  
## Simulator

At a high level, the Simulator has three major components:

  1. A 3D model of the stairwell and displaying with high quality
     graphics. In the best case, the dichroic of the wings should
     be accurately captured (reflection and shadow). This involves
     rendering the current state of Fractal Flyers (e.g., wing positions).
  2. Systems code to have the simulator appear as 76 Fractal Flyers
     (serial ports and flash drives), and signaling when the files change
     or new scripts arrive. You will likely want to use CUSE and FUSE
     (character-device userspace and filesystem userspace) in Linux
  3. Simulating execution of Fractal Flyer Python code, so that it
     changes the internal state of the simulator to reflect what
     the Engine is specifyin. This involves coorperating with the
     Engine group to decide on the APIs.

## Handing in

There are four due dates for this assignment.
  1. By class time on Tuesday, April 21st, your group should have
     come up with a first design decomposition of your modules and
     their dependencies. These should be at the level of files and classes,
     not high-level abstractions. E.g., if you need a linked list, there
     should be list module. This module might be supplied by an existing
     library, but it is a module. Write up these decompositions are
     figures (e.g., slides) which we will all look at and discuss in
     class on Tuesday. You can decide how to present: you can have a 
     lead, break the presentation into three parts, or do something else.
     I expect this to take 4-5 hours of working together.
  1. By end-of-day Saturday, April 25th, your group should have 
     initial designs of
     the APIs of each module. This includes a high-level description
     of what the module does, as well as functions or methods. Share
     these designs with everyone else in your group, for...
  1. By end-of-day Monday, April 27th, your group should have 
     committed source code
     that defines the key APIs in your part of the project. These key APIs
     are the one that aren't internal to a sub-project: they are the
     dependencies that reach across people. These APIs should be internally
     consistent: sub-part A should not assume you use one set of data
     structures while sub-part B assumes you use others.
  1. In class on Tuesday, April 28th, your group will spend 10 minutes
     presenting your overall design. Commit your presentation slides
     as a PDF to the repository after your presentation, including
     any feedback you receive. This class is a design review for your
     software.


For committing source code, I will send instructions early next week. I'll 
also outline how we will interact through GitHub to discuss and revise
software.
