# The mechanical CAD file place

This is the folder for mechanical CAD files. This "top level" is a great place for graphical things of general interest. For example, snapshots of things or illustrations for the object might be good things for this level. Also, at this level we might put animations of the entire object and so forth.

The sub-folders are for major sub-assemblies and their component parts. So we have folders "top_plate", "body_support_flange", and "wings". The idea is that the "top_plate" will become a major sub-assembly with other sub-assemblies and components attached to it. The top_plate and all of the stuff attached to it would go in the "top_plate" folder.

The folder "Mech_Comp_Data" is where you'll find datasheets (and 3D CAD models) for components that we purchase and use in the art object. These are collected in this folder rather than listed out separately with the assembly.

Please let one of the teaching staff know if you have questions, or this seems awkward, or if you see a better way to organize things.

Thanks!


# Hierarchy

    SCULPTURE
    |
    |
    |-------WINGS
    |
    |
    |-------ENTIRE_BODY
    |       |
    |       |
    |       |---------TOP_PLATE
    |       |
    |       |
    |       |---------TOP_FLANGE
    |       |         |
    |       |         |
    |       |         |---------KEYHOLE_FASTENERS
    |       |
    |       |
    |       |---------VACUUM_FORMED_BODY
    |       |
    |       |
    |       |---------[[add wing attachment and actuation stuff here]]
    |       |
    |       |
    |       |
    |       |


 
    