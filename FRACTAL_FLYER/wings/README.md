# Wing Etchings

This directory contains files for wing etching. Each wing has a fractal
pattern on it:

![Wing fractal](wing-fractal-only.png)

This fractal pattern was generated with the [geomandel](https://github.com/crapp/geomandel) program, using the [make-julia.sh](make-julia.sh) script
in this directory. This Julia set output was then fed through the
[image-to-wing.py](image-to-wing.py) program to crop it and change it from
regions of black to the boundary around regions of black.


