| #    | Category                 | Theme                                     | Words                                                        | Icon                               |
| ---- | ------------------------ | ----------------------------------------- | ------------------------------------------------------------ | ---------------------------------- |
|      |                          |                                           |                                                              |                                    |
| 1    | In Memory Of             | Harris  Ryan                              | Harris Ryan: Established Department                          | harris-crt                         |
| 2    |                          | Ward  Kindy                               | Ward Kindy: Power Generation                                 | transformer                        |
| 3    |                          | Hugh  Skilling                            | Hugh Skilling: Engineering Educator                          | book                               |
| 4    |                          | Frederick  Terman                         | Frederick Terman: Silicon Valley                             | silicon_valley                     |
| 5    |                          | John  Linvill                             | John Linvill: Transistorized Curriculum                      | transistor                         |
| 6    |                          | Gordon  Kino                              | Gordon Kino: Confocal Microscopy                             | confocal_microscope                |
| 7    |                          | Anthony  Siegman                          | Anthony Siegman: Lasers & Masers                             | laser                              |
| 8    |                          | Calvin  Quate                             | Calvin Quate: Nanoscience & Microscopy                       | atomic_force_microscopy            |
| 9    |                          | Edward  McCluskey                         | Edward McCluskey: Digital Design                             | multivalue_logic_circuits          |
| 10   |                          | Thomas  Cover                             | Thomas Cover: Superposing Signals                            | broadcast_channel                  |
| 11   |                          | Hector  Garcia-Molina                     | Hector Garcia-Molina: Mentor to All                          | stanford_digital_library_project   |
| 12   | Notable Alumni           | Martin  Hellman                           | Martin Hellman: Public Key Cryptography                      | public_key_cryptography            |
| 13   |                          | John  Hennessy                            | John Hennessy: Computer Architecture                         | mips_architecture                  |
| 14   |                          | Jen-Hsun  Huang                           | Jen-Hsun Huang: nVIDIA                                       | nVIDIA                             |
| 15   |                          | Jerry  Yang                               | Jerry Yang: Yahoo!                                           | yahoo                              |
| 16   |                          | David  Filo                               | David Filo: Yahoo!                                           | yahoo                              |
| 17   |                          | Vint  Cerf                                | Vint Cerf: Father of the Internet                            | internet                           |
| 18   |                          | Larry  Page                               | Larry Page: Google                                           | google                             |
| 19   |                          | Sergey  Brin                              | Sergey Brin: Google                                          | google                             |
| 20   |                          | Andy  Bechtolsheim                        | Andy Bechtolsheim: Sun Microsystems                          | sun_microsystems                   |
| 21   |                          | Sandy  Lerner                             | Sandy Lerner: Cisco Systems                                  | cisco                              |
| 22   |                          | John  Cioffi                              | John Cioffi: Father of DSL                                   | dsl                                |
| 23   |                          | Thomas  Kailath                           | Thomas Kailath: Control Theory                               | linear_systems                     |
| 24   |                          | Ray  Dolby                                | Ray Dolby: Dolby Laboratories                                | dolby                              |
| 25   |                          | Ted Hoff                                  | Ted Hoff: Microprocessor                                     | microprocessor                     |
| 26   |                          | Bill  Hewlett                             | Bill Hewlett: HP                                             | hp                                 |
| 27   |                          | David  Packard                            | David Packard: HP                                            | hp                                 |
| 28   | Departmental Initiatives | Stanford  Prototyping Facility            | SPF: System Prototyping Facility                             | stanford_logo                      |
| 29   |                          | SystemX  Alliance                         | SystemX Alliance                                             | SystemXLogo_bw                     |
| 30   |                          | Computer  Forum                           | Computer Forum                                               | stanford_computer_forum            |
| 31   |                          | Q-Farm:  Stanford-SLAC Quantum Initiative | Q-Farm: Stanford-SLAC Quantum Initiative                     | Qfarm_logo_selection               |
| 32   |                          | Stanford  Student Space Initiative        | Stanford Student Space Initiative                            | sssi                               |
| 33   |                          | Stanford  Student Robotics Club           | Stanford Student Robotics Club                               | sfsrobotics_bw                     |
| 34   |                          | Lab64  Makerspace                         | Lab64 Makerspace                                             | Lab64_bw                           |
| 35   |                          | SNF:  Stanford Nanofabrication Facility   | SNF: Stanford Nanofabrication Facility                       | snf                                |
| 36   |                          | MIPS  Processor                           | MIPS Processor                                               | MIPSBW                             |
| 37   |                          | Stanford  Computational Imaging Lab       | Stanford Computational Imaging Lab                           | sci-logo                           |
| 38   | Current Faculty/ Staff   | Umran  Inan                               | Umran Inan: VLF Electromagnetic Radiation                    | radio-antenna                      |
| 39   |                          | Dorsa  Sadigh                             | Dorsa  Sadigh: Autonomous Systems                            | autonomous-smart-car               |
| 40   |                          | Chelsea  Finn                             | Chelsea  Finn: Robotic Intelligence                          | robotic-intelligence               |
| 41   |                          | Jeannette  Bohg                           | Jeannette Bohg: Robot Mesmeriser                             | bohg_icon                          |
| 42   |                          | John  Duchi                               | John  Duchi: Optimization and Beyond                         | convex_function_illustration       |
| 43   |                          | Juan  Rivas-Davila                        | Juan  Rivas-Davila: Electrical Power                         | resonant-converter                 |
| 44   |                          | Mary  Wootters                            | Mary  Wootters: Theoretical Aspects of Engineering           | algorithms                         |
| 45   |                          | H. -S.  Philip Wong                       | H.-S. Philip Wong: Nanoelectronics systems                   | Philip_Wong_Icon                   |
| 46   |                          | Joseph  Little                            | Joseph  Little: Any sufficiently advanced research is indistinguishable from magic | DiscWorld                          |
| 47   |                          | Denise  Murphy                            | Denise Murphy: Departmental Memory                           | recordkeeping                      |
| 48   | Project Members          | Phil  Levis                               | Philip Levis: Engineer                                       | phil-levis-icon                    |
| 49   |                          | Mark  Horowitz                            | *Mark  Horowitz: FLIGHT Project Mentor*                      | *stanford_logo*                    |
| 50   |                          | Charlie  Gadeken                          | Charles A. Gadeken: Artist                                   | charlie_icon                       |
| 51   |                          | Steven  Clark                             | *Steven  Clark: FLIGHT Project Mentor*                       | *stanford_logo*                    |
| 52   |                          | Kathy  Richardson                         | Kathy J Richardson, PhD '94: Engineering ≡ Art ≡ Constraint Optimization | FractalFlyerKJRicon                |
| 53   |                          | Matt  Trost                               | Matthew Trost: Embedded  Enthusiast                          | 199_logo                           |
| 54   |                          | Luke Hsiao                                | Luke Hsiao: Raising Aspirations                              | neXt                               |
| 55   |                          | Michal  Adamkiewicz                       | *Michal  Adamkiewicz: FLIGHT Project Member*                 | *stanford_logo*                    |
| 56   |                          | Mihir  Garimella                          | Mihir Garimella: CS ’21                                      | tesseract                          |
| 57   |                          | Claire  Huang                             | Claire Huang: Computer Science '20                           | claire_logo                        |
| 58   |                          | Sean  William Konz                        | Sean Konz: Remember and Celebrate                            | seanKonz_icon                      |
| 59   |                          | Lee  Marom                                | Lee Marom: Designer, Artist, Engineer \| PRL 2020            | lee_logo                           |
| 60   |                          | David  Mendoza                            | *David  Mendoza FLIGHT Project Member*                       | *stanford_logo*                    |
| 61   |                          | Andrea  Nari Stein                        | Andrea Stein, engineer, PRL 2020                             | andrea_logo                        |
| 62   |                          | Will  Charles Thompson                    | Will Thompson: “Big Gulps, huh?”                             | WillThompson Logo                  |
| 63   |                          | Tim Paul  Vrakas                          | Tim Paul  Vrakas: Per Aspera Ad Astra                        | SSI-Logo-Only-Negative             |
| 64   |                          | Kelly  Woo                                | Kelly Woo: Researching Wide Band Gap Devices                 | kelly_icon                         |
| 65   |                          | Sydney  Marler                            | Sydney  Marler: Per Aspera Ad Astra                          | constellation-transparent-hercules |
| 66   |                          | Charles  Tsao                             | Charles Tsao: Class of 2021                                  | charles_logo                       |
| 67   |                          | Eric  William Colbert                     | Eric Colbert: World Wanderer                                 | eric_colbert_icon                  |
| 68   |                          | Hudson  Randal Ayers                      | Hudson Ayers: Tock OS                                        | esys                               |
| 69   |                          | Carly  Aubrie Davenport                   | Carly Davenport: Diversity in STEM                           | flamingBWbunni                     |
| 70   |                          | Ron  Domingo                              | Ron Domingo: B.S. EE 2019 \| M.S. EE 2020                    | ron_icon                           |
| 71   |                          | Hallie  Dunham                            | Hallie Dunham: EE BS/MS, History Minor                       | Hallie_Icon                        |
| 72   |                          | Courtney  Noel Moran                      | Courtney Moran: @cocomoro105                                 | pinnaple                           |
| 73   |                          | Vinh  Quang Nguyen                        | Vinh Quang Nguyen, '19: LED Diffusion                        | ee125_icon_vnguyen5                |
| 74   |                          | Omar  Palacios Orbe Sr                    | Omar Palacios: Wisdom, Courage, Power                        | royal_crest                        |
| 75   |                          | Viraga  Perera                            | Viraga Perera: MS EE '20                                     | Perera_Icon                        |
| 76   |                          | Erik  Kevin Van                           | Erik Van: Community Building Engineer                        | erik_icon                          |
| ??   |                          | Moses Swai                                | Moses Swai: BS'18\MS'20 ME, "Irie! Hakuna Matata"            | moses_logo                         |



Note: Those entries in *italics* are subject to change based on information to be received from the related individual